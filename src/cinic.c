// Configuration options 
// Beallitasi lehetosegek
//#define DEBUG2
//#define DEBUG3
//#define DEBUG_CONFIG
//#define DEBUG_CON
#define DEFAULT_SERVER "127.0.0.1"
#define DEFAULT_PORT	6667
#define DEFAULT_NICK "CINIC"
#define DEFAULT_OTHER_NICK "CIN"
//Ha a cinic nicknev nem elerheto veletlenszeruen general egy masikat,
//amely a fenti karaktersorrak kezdodik
#define DEFAULT_CHUNK_SIZE 	390
/*390 a maximalis ertek, 1 a min. A kodolastol 
fuggoen a tenyleges chunk meret DEFAULT_CHUNK_SIZE+1 
nagysagu is lehet, ami a helyes mukodes resze*/
#define DEFAULT_RECONNECT	1
//1 jelenti igen, 0 jelenti nem. BARMELY MAS ERTEK HIBAHOZ VEZETHET!!!!!!!!
//1 means yes, 0 means no. Any other value will lead to undefined actions
#define DEFAULT_RECONNECTIONS	3
//Ha bomlik a kapcsolat ennyiszer probaljon ujra csatlakozni
//try DEFAULT_RECONNECTIONS times to reconnect. DEFAULT_RECONNECTIONS >=0
#define DEFAULT_LIMIT_RECONNECTIONS	1
//1 jelenti igen, 0 jelenti nem. BARMELY MAS ERTEK HIBAHOZ VEZETHET!!!!!!!!
//1 means yes, 0 means no. Any other value will lead to undefined actions
#define DEFAULT_REAL_NAME	"CINIC:\040CINIC\040Is\040No\040IRC\040Client"
//Maximalisan 490 karakter hosszu!
//Maximal 490 characters long!
#define COMMAND_CHAR	"/"
//Verbose modeban, ez a karakter jelenti, hogy nem az IRC szervernek irunk,
//hanem a programnak
#define DEFAULT_USABLE_CHARS "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
//a veletlenszeru szoveggeneraloshoz, nicknev es ID
//for random string generation, for nicks, ID-s
#define DEFAULT_WHITESPACE_CHARS "\040\f\n\r\t\v"
//szavak szeparalasara hasznalt karakterek
// for command separation
#define DEFAULT_DELAY 3
//Egyszeru FLOOD vedelem: ennyi masodpercet varakozik uzenetek kuldese kozott
//Simple FLOOD protection: seconds to wait between messages
#define VERBOSE FALSE
#define DATA_COMMAND "D"
#define OUT_COMMAND "O"
#define CLOSE_COMMAND "C"
#define ERROR_COMMAND "E"
#define REPORT_COMMAND "R"
#define TUNNEL_COMMAND "T"
#define PILLEROUT_COMMAND "PO"
#define PILLERERROR_COMMAND "PE"
#define PILLERREPORT_COMMAND "PR"
#define EXTERNAL_TUNNEL_COMMAND "E"
#define LIST_COMMAND "list"
#define CONF_LIST_COMMAND "listconf"
#define DELAY_COMMAND "delay"
#define VERBOSE_COMMAND "verbose"
#define QUIT_COMMAND "quit"
#define SERVER_COMMAND "server"
#define ID_LENGTH 10
#define ERROR_CODE_READ_SOCKETCLOSED "001"
#define ERROR_CODE_WRITE_SOCKETCLOSED "002"
#define ERROR_CODE_DATA_WRONGUSAGE "003"
#define	ERROR_CODE_DATA_IDTOOLONG "004"
#define ERROR_CODE_DATA_NOSEPARATOR "005"
#define ERROR_CODE_DATA_NOCONNECTION "006"
#define ERROR_CODE_OUT_WRONGUSAGE "007"
#define ERROR_CODE_OUT_IDTOOLONG "008"
#define ERROR_CODE_OUT_WRONGPORT "009"
#define ERROR_CODE_OUT_NICKIDINUSE "010"
#define ERROR_CODE_OUT_WRONGHOST "011"
#define ERROR_CODE_OUT_SOCKWONTOPEN "012"
#define ERROR_CODE_OUT_COULDNOTCONNECT "013"
#define ERROR_CODE_CLOSE_WRONGUSAGE "014"
#define ERROR_CODE_CLOSE_IDTOOLONG "015"
#define ERROR_CODE_CLOSE_NOCONNECTION "016"
#define ERROR_CODE_REPORT_NOCONNECTION "017"
#define ERROR_CODE_REPORT_SOCKWONTOPEN "018"
#define ERROR_CODE_REPORT_COULDNOTBIND "019"
#define ERROR_CODE_REPORT_COULDNOTLISTEN "020"
#define REPORT_CODE_OUT_SUCCESS "001"
#define REPORT_CODE_CLOSE_SUCCESS "002"
#define PILLERERROR_CODE_PILLERREAD_SOCKETCLOSED "001"
#define PILLERERROR_CODE_PILLERWRITE_SOCKETCLOSED "002"
#define PILLERERROR_CODE_PILLEROUT_WRONGHOST "003"
#define PILLERERROR_CODE_PILLEROUT_NICKIDINUSE "004"
#define PILLERERROR_CODE_PILLEROUT_WRONGUSAGE "005"
#define PILLERERROR_CODE_PILLEROUT_IDTOOLONG "006"
#define PILLERERROR_CODE_PILLEROUT_WRONGPORT "007"
#define PILLERERROR_CODE_PILLEROUT_CWRONGHOST "008"
#define PILLERERROR_CODE_PILLEROUT_CSOCKWONTOPEN "019"
#define PILLERERROR_CODE_PILLEROUT_CCOULDNOTCONNECT "010"
#define PILLERERROR_CODE_PILLEROUT_DWRONGHOST "011"
#define PILLERERROR_CODE_PILLEROUT_DSOCKWONTOPEN "012"
#define PILLERERROR_CODE_PILLEROUT_DCOULDNOTCONNECT "013"
#define PILLERERROR_CODE_PILLERREPORT_NOCONNECTION "014"
#define PILLERREPORT_CODE_PILLEROUT_SUCCESS "001"
#define LOGO_NAME "../conf/cinic.logo"
#define CONFIG_FILE_NAME "../conf/cinic.conf"
#define POST_CONNECTION_COMMANDS_FILE_NAME "../conf/postcon.conf"


#include <sys/socket.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>


typedef enum {FALSE=0, TRUE} boolean;

typedef struct y_struct
    {
    	char* data;
	unsigned int size;
    	struct y_struct *next; 
    } output_FIFO_item_type;

typedef struct x_struct
    {
	char ID[ID_LENGTH+1];
	int fd;
	unsigned short port;
	unsigned short host_port;
	char nick[10];
	char host[64];
	output_FIFO_item_type *output_FIFO;
	struct x_struct *partner;
        struct x_struct *next; 
    } socket_list_item_type;

output_FIFO_item_type* IRC_data_FIFO = NULL; 
output_FIFO_item_type* IRC_controller_FIFO = NULL;

socket_list_item_type *socket_list = NULL;
socket_list_item_type *piller_socket_list = NULL;
int sock_number = 0;
int piller_sock_number = 0;

int IRC_sock_fd = -1;
boolean work = TRUE;
boolean data_tr_enabled = FALSE;
boolean make_IRC_connection = TRUE;

char IRC_nick[10];
char IRC_server[64];
char IRC_real_name[511];
unsigned short IRC_port = DEFAULT_PORT;

unsigned short connection_counter;
char IRC_other_nick[4];
unsigned short chunk_size;
boolean reconnect;
unsigned short reconnections;
boolean limit_reconnections;
short connection_state = 0;
char command_char;

fd_set readfs, writefs;
char temp_char, IRC_in_buffer[514];
char* IRC_in_buffer_pointer; 
char IRC_out_buffer[513];
char stdout_buffer[513];
time_t  w_time, s_time;
struct timeval tv;
unsigned short delay;
boolean verbose;

int nagyon_atmeneti_valtozo;


char* generate_random_string(unsigned short min, unsigned short max, char* usable_characters)
 {
    unsigned short j,l,i;
    char* temp_string; 
    
    
    srand((unsigned int)time((time_t *)NULL));
    l = strlen(usable_characters);
    j = min + floor( (double) (rand() / ( RAND_MAX/(max-min+1) )) );
    temp_string = (char *) malloc( sizeof(char) * (j+1) );
    for (i=0; i<j; i++) 
	temp_string[i] = usable_characters[(int) floor( rand() / ( RAND_MAX/l ) )];
    temp_string[j] = '\0';
    
    return temp_string;
 }


short make_connection(char *hostname, u_short port, int* s)
 {
    struct sockaddr_in sa;
    struct hostent *hp;
//    int s;


    *s = -1;
    if ((hp = gethostbyname(hostname)) == NULL)
	return -1;
    memset(&sa, 0, sizeof(sa));
    bcopy(hp->h_addr, (char *) &sa.sin_addr, hp->h_length);
    sa.sin_family = hp->h_addrtype;
    sa.sin_port = htons((u_short) port);
    if ((*s = socket(hp->h_addrtype, SOCK_STREAM, 0)) < 0)
	return -2;
    fcntl(*s, F_SETFL, O_NDELAY);
    	
    if (connect(*s, (struct sockaddr *) &sa, sizeof(sa)) < 0) 
     {
	if (errno == EINPROGRESS) return -4;
	else
	 {
	      close(*s);
	      *s = -1;
	      return -3;
	 }
     }
        
    return 0;
 }


int make_connection_server(u_short port)
 {
    struct sockaddr_in sa;
    int s;


    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr= htonl(INADDR_ANY);
    sa.sin_port = htons((u_short) port);
    if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	return -1;
    fcntl(s, F_SETFL, (O_NDELAY | fcntl(s,F_GETFL)) );
    if ( bind(s,(struct sockaddr *) &sa, sizeof(sa))< 0	)
        return -2;
    if ( listen(s, 1) < 0 )
	return -3;
    
   return s;
 }


void FIFO_add_item(output_FIFO_item_type** FIFO_point, char *data, unsigned int size)
 {
    output_FIFO_item_type *temp_FIFO_item, *last_FIFO_item;


    temp_FIFO_item = (output_FIFO_item_type *) malloc( sizeof(output_FIFO_item_type));
    
    temp_FIFO_item->next = NULL;
    temp_FIFO_item->data = (char *) malloc( size );
    memcpy(temp_FIFO_item->data, data, size);
    temp_FIFO_item->size = size;
    
    if ( *(FIFO_point) )
     {
	last_FIFO_item = (*FIFO_point);
        while ( last_FIFO_item->next ) last_FIFO_item = last_FIFO_item->next;

	last_FIFO_item->next = temp_FIFO_item;
     }	 
    else *(FIFO_point) = temp_FIFO_item;
 }


void FIFO_del_first_item(output_FIFO_item_type **FIFO_point)
 {
    output_FIFO_item_type *temp_FIFO_item;		

    temp_FIFO_item = *FIFO_point;
    *FIFO_point = (*FIFO_point)->next;
    free(temp_FIFO_item->data);
    free(temp_FIFO_item);			
 }


int FIFO_write(output_FIFO_item_type **FIFO_point, int fd, boolean write_to_stdout, time_t* write_time)
 {
    ssize_t byte_counter;


    if (*FIFO_point)
     {
	if (write_time) time(write_time);
	if
	 (
	     ( 
		byte_counter 
		= 
		write(fd,
				(*FIFO_point)->data,
				(*FIFO_point)->size )
	     )
	    ==
	    (*FIFO_point)->size
	 )	
	 {

	    if (write_to_stdout)
	     {
		printf(
		  	 "*** Kiirtam %d fd re a kovetkezot:", fd );
	
		if (write_to_stdout) write(STDOUT_FILENO, (*FIFO_point)->data, byte_counter );

		printf(
		  	 "\n" );
	
	     }


	    FIFO_del_first_item(FIFO_point);
	 }
	else
	 {
	    if ( byte_counter > 0)
	     {

		if (write_to_stdout)
		 {

		    printf(
		  	 "*** Kiirtam %d fd re a kovetkezot:", fd );
	
		    if (write_to_stdout) write(STDOUT_FILENO, (*FIFO_point)->data, byte_counter );


		    printf(
		  	 "\n" );
	
		 }

// ...igazitom a meretet
		(*FIFO_point)->size = (*FIFO_point)->size - byte_counter; 

// A hatul levo bytokat elori irom...
		memmove((*FIFO_point)->data, (*FIFO_point)->data + byte_counter * sizeof(char), (*FIFO_point)->size);
 	     }
	    else
		if 
		 (
		    !byte_counter
		    ||
		     (
			(byte_counter < 0) && (errno != EAGAIN) 
		     )	
		 ) return -1;
	    return 1;
	 }

     }

    return 0;
 }


int FIFO_flush(output_FIFO_item_type **FIFO_point, int fd, boolean write_to_stdout, time_t* write_time)
 {

    while (*FIFO_point)
     {

	switch (FIFO_write(FIFO_point, fd, write_to_stdout, write_time) )
	 {
	    case -1: return 1;
	    case 1: return 0;

	 }
     }

    return 0;
 }

char* string_up(char* input)
 {
    char* temp_string;
    unsigned short i, input_length;
    

    if (!input) return NULL;
 
    input_length = strlen(input);
    temp_string = (char *) malloc( sizeof(char) * (input_length + 1) );
    for(i=0; i < input_length; i++) temp_string[i] = toupper(input[i]);
    temp_string[input_length] = '\0';

    return temp_string;
 }


socket_list_item_type* find_socket(socket_list_item_type* socket_list, int socket_number, char *ID, char* nick)
 {
    socket_list_item_type *temp_socket_item;
    char* nick_upper;
    char* socket_nick_upper;
    int i;
    

#ifdef DEBUG2
printf("Adatbazis merete:%d\n", socket_number);
printf("Keresem az adatbazisban:%s, %s\n", ID, nick);
#endif

    nick_upper = NULL;
    socket_nick_upper = NULL;

    temp_socket_item = socket_list;
    for (i=0; i < socket_number; i++)
     {
	nick_upper = string_up(nick); 
	socket_nick_upper = string_up(temp_socket_item->nick);
     
	if 
	 (
	    (nick ? !( strcmp(nick_upper, socket_nick_upper) ) : 1)
	    &&
	    (ID ? !( strcmp(ID, temp_socket_item->ID) ) : 1)
	 )
	 {
	    free(nick_upper);
	    free(socket_nick_upper);

	    return temp_socket_item;
	 }

	free(nick_upper);
	free(socket_nick_upper);
	    
	temp_socket_item = temp_socket_item->next;
     }

    return NULL;
 }


void add_socket(socket_list_item_type** socket_list_point, 
		int* socket_number_point, 
		char *ID, 
		int fd, 
		char* nick, 
		unsigned short port, 
		char* host, 
		unsigned short host_port, 
		socket_list_item_type *partner)
 {
    socket_list_item_type *temp_socket_item;

#ifdef DEBUG2
printf("Hozzaveszek egy bejegyzest a socket listahoz:%s, %d, %s, %d, %s, %d\n",ID , fd, nick, port, host, host_port);

#endif
    temp_socket_item = (socket_list_item_type *) malloc( sizeof(socket_list_item_type));

    strcpy(temp_socket_item->ID, ID);
    strcpy(temp_socket_item->nick, nick);
    strcpy(temp_socket_item->host, host);
    temp_socket_item->fd = fd;    
    temp_socket_item->port = port;
    temp_socket_item->host_port = host_port;
    temp_socket_item->partner = partner;
    temp_socket_item->output_FIFO = NULL;

    if (*socket_list_point)
     {
	temp_socket_item->next = (*socket_list_point)->next;
	(*socket_list_point)->next = temp_socket_item;
     }
    else 
     {
	temp_socket_item->next = temp_socket_item;
		
     }
    (*socket_list_point) = temp_socket_item;
    
    (*socket_number_point)++;
 }
 
 
boolean remove_socket(socket_list_item_type** socket_list_point, int* socket_number_point, socket_list_item_type* remove_item)
 {
    socket_list_item_type *temp_socket_item;
    int i;


    if (remove_item)
     {
	close(remove_item->fd);
	temp_socket_item = remove_item;

	if ((*socket_number_point) > 1)
	 {
	    for (i=1; i < (*socket_number_point); i++) remove_item = remove_item->next;
	    remove_item->next = temp_socket_item->next;
	    remove_item = remove_item->next;
	    if ((*socket_list_point) == temp_socket_item) (*socket_list_point) = remove_item;
/*kiszedtuk a lancbol az elemet*/
	 }
	else (*socket_list_point) = NULL;
	
	while (temp_socket_item->output_FIFO) FIFO_del_first_item(&temp_socket_item->output_FIFO);
	free(temp_socket_item);
/*memoriahely felszabaditva*/
	(*socket_number_point)--;
	return TRUE;
     }

    return FALSE;
 }
 
 
void collect(char** string_pointer, char read_char, char* stop_chars)
 {
    if ( *( *string_pointer + sizeof(char) ) )
     {
	*( *string_pointer + sizeof(char) ) = '\0';
	if ( !strchr(stop_chars, read_char) )
	 {
	    *( *string_pointer ) = read_char;
	    *string_pointer = *string_pointer + sizeof(char);
	 }
     }
 }


char* get_word( char* buffer, unsigned short word_number, 
    unsigned short* word_length, char* separator_chars, unsigned short size )
 {
    unsigned short word_counter = 0, begin, end;
    

    if (size > strlen(buffer)) size = strlen(buffer);
    end = 0;
    begin = 0;

    if (end < size)
     {
	while( word_number > word_counter)
	 {
	    begin = end;
	    word_counter++;
	    while (  strchr(separator_chars, buffer[begin] ) && (begin < size) ) begin++;
	    end = begin;
	    while (  !strchr(separator_chars, buffer[end] ) && (end < size) ) end++;
	 }
     }

    if (!(*word_length = end-begin)) return NULL;
    else return buffer + sizeof(char) * begin;
 }


void* decode(char* input, unsigned short* output_size_point)
 {
    char* output;
    unsigned short input_size, i, j, state;
/*
%n -> \n
%r -> \r
%0 -> \0
%% -> %
*/
    

    input_size = strlen(input);
    
    output = (char *) malloc( input_size * sizeof(char));
    state = 0;
    i = 0;
    j = 0;
    
    while (i < input_size )
     {

	output[j] = input[i];
	if ( (! state) && (input[i] == '%')) state = 1;
	else if (state == 1)
	 {
	    state = 0;
		    j--;
		    switch (input[i])
		     {
			case 'n':
			 {
			    output[j] = '\n';
			    break;
			 }
			case 'r':
			 {
			    output[j] = '\r';
			    break;
			 }
			case '0':
			 {
			    output[j] = '\0';
			    break;
			 }
			case '%':
			 {
			    output[j] = '%';
			    break;
			 }

			default:
			    j++;
		     }		
		 }
	j++;
	i++;
     }

    *output_size_point = j;

    return output;
 }


int parse_post_connection_commands_file()
 {
    int	fd, j, i, k;
    unsigned short	state = 0;
    char read_buffer[513];


    k = 0;
//    IRC_out_buffer[k] = '\0';
    if ( (fd = open(POST_CONNECTION_COMMANDS_FILE_NAME, O_RDONLY)) >= 0 )
     {
        while ((j = read(fd, read_buffer, sizeof(read_buffer)) ) > 0)
	 {
	    for (i = 0; i < j; i++)
	     {


		if (state == 0)	
		 {
		    if (read_buffer[i] == '#') state = 2;
		    else if (read_buffer[i] != '\n')
		     {
			IRC_out_buffer[k] = read_buffer[i];
			IRC_out_buffer[k+1] = '\r';
			IRC_out_buffer[k+2] = '\n';
			IRC_out_buffer[k+3] = '\0';

			k++;
			
			state = 1;
		     }
		 }
		else if (state == 1)	
		 {
		    if (read_buffer[i] == '\n')
		     {
			FIFO_add_item(&IRC_controller_FIFO, 
				IRC_out_buffer, 
				strlen(IRC_out_buffer) * sizeof(char));
			k = 0;

			state = 0;
		     }
		    else
		     {
			IRC_out_buffer[k] = read_buffer[i];
			IRC_out_buffer[k+1] = '\r';
			IRC_out_buffer[k+2] = '\n';
			IRC_out_buffer[k+3] = '\0';

			k++;

			if (k >= 510)
			 {
			    
			    FIFO_add_item(&IRC_controller_FIFO, 
				IRC_out_buffer, 
				strlen(IRC_out_buffer) * sizeof(char));

			    k = 0;
			    state = 2;
			 }
			
		     }
		 }
		else
		 {
		    if (read_buffer[i] == '\n')	state = 0;
		 }

	     }
	 }
	 
	if (state == 1)
	    FIFO_add_item(&IRC_controller_FIFO, 
				IRC_out_buffer, 
				strlen(IRC_out_buffer) * sizeof(char));
	
	close(fd);

	if (j < 0) 
	 {
	    printf("*** Post connection commands file olvasasi hiba. (%s)\n", POST_CONNECTION_COMMANDS_FILE_NAME );
	    return 2;
	 }

	return 0;
     }
    else
     {
	printf("*** Post connection commands file-t nem talaltam. (%s)\n", POST_CONNECTION_COMMANDS_FILE_NAME );
	return 1;
     }
 }


short num_state = 0;

boolean IRC_command_interpretation(char* words[9], char* command_buffer, unsigned short word_counter)
 {
    unsigned short word_length; 
    char sender_nick[9];
    char* ID;
    char* temp_string;
    unsigned short i = 0;
    int fd;
    socket_list_item_type* temp_socket_item;
    char* data;

//Got no words? Get ta hell outta here!
//Ha nincsenek szavak, akkor vege 
    if (! word_counter ) return TRUE;
    
//get the senders nick
//kiemeljuk az elso szobol a kuldo nickjet
    temp_string = get_word(words[0],
		1,
		&word_length,
		":%@!",
		strlen(words[0]));

    strncpy( sender_nick, temp_string, word_length );
    sender_nick[word_length] = '\0';

//command interpretation starts here
//parancsok ertelmezese
    if ( !strcmp(words[1], "001") )
     {
/*minden rendben, mukodes indulhat*/
	if (!data_tr_enabled) data_tr_enabled = TRUE;

//post-connection parancsok olvasasa
	parse_post_connection_commands_file();
	
	printf( "*** IRC kapcsolat sikeresen kiepult. nick: %s\n", IRC_nick );
	
	return TRUE;
     }

    if ( !strcmp(words[1], "433" ) )
     {
//our nick is already in use, generate a new one
//a nickunket mar felhasznaltak, veletlenszeruen generalunk egy ujat
	strcpy(stdout_buffer, IRC_other_nick);
	temp_string = generate_random_string(7-strlen(IRC_other_nick), 8-strlen(IRC_other_nick), DEFAULT_USABLE_CHARS);
	
#ifdef DEBUG2
printf("%s %d %d\n", temp_string,7-strlen(IRC_other_nick), 8-strlen(IRC_other_nick));
#endif
	strcat(stdout_buffer, temp_string); 
	free(temp_string);

	sprintf(IRC_out_buffer, "NICK %s\r\n", stdout_buffer);
	FIFO_add_item(&IRC_controller_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

	printf( "*** A valasztott nick mar hasznalatban van. Probalkozom egy masikkal...\n"   );
	
	return TRUE;
     }

    if ( !strcmp(words[0], "PING") )
     {
//PING
	sprintf(IRC_out_buffer, "PONG %s\r\n", (words[1] + sizeof(char)) ); 
		    
	FIFO_add_item(&IRC_controller_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

	printf( "*** PING? PONG!\n"   );
	
	return TRUE;
     }

    if ( (!strcmp(words[1], "NICK")) && (!strcmp(sender_nick, IRC_nick)))
     {
//UJ nick elfogadva
	strcpy(IRC_nick, words[2] + sizeof(char));	

	printf( "*** Uj nick elfogadva: %s\n", IRC_nick );
	
	return TRUE;
     }
     
    if ( !strcmp(words[1], "401" ) )
     {
//No such nick, remove everything from data base, that is involved with this nick
//Ilyen nevu felhasznalo nincs, igy a socketlistabol kitoroljuk
	while ( remove_socket( &socket_list, &sock_number, find_socket( socket_list, sock_number, NULL, words[3] ) ) ) { }

	printf( "*** %s nevu felhasznalo nincs, ha a listaban szerepel, torlom.\n", words[3]);
	
	return TRUE;
     }

    if ( strcmp(words[1], "PRIVMSG" ) ) return FALSE;

    if ( !strcmp(words[3] + sizeof(char), DATA_COMMAND ) )
     {
/*
words[0] -> sender_nick!username@host
words[1] -> PRIVMSG
words[2] -> reciever_nick
words[3] -> DATA_COMMAND
words[4] -> ID
words[5] -> :datastartshere....
words[6] ->
words[7] ->
words[8] ->

*/

	if (word_counter < 6)
	 {
/*nincs kello szamu parameter*/
	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s %s Adatkuldes helytelen. \"%s ID :data\"\r\n", 
			sender_nick,
			ERROR_COMMAND, 
			words[4],
			ERROR_CODE_DATA_WRONGUSAGE,
			DATA_COMMAND);
	    FIFO_add_item(&IRC_data_FIFO, 
			IRC_out_buffer, 
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen adatkuldest kezdemenyezett. (Parameterszam nem megfelelo)\n", sender_nick);
	
	    return FALSE;
	 }

	if (strlen(words[4]) > ID_LENGTH)
	 {
//Az ID parameter tulsagosan hosszu
	    words[4][ID_LENGTH] = '\0';

	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s... %s %d lehet az ID maximalis hosszusaga.\r\n",
			sender_nick,
			ERROR_COMMAND,
			words[4],
			ERROR_CODE_DATA_IDTOOLONG,
			ID_LENGTH);
	    FIFO_add_item(&IRC_data_FIFO,
			IRC_out_buffer,
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen adatkuldest kezdemenyezett. (ID hossza nem megfelelo)\n", sender_nick);
	
	    return FALSE;
	 }

	if ( *(words[5]) != ':' )
	 {
//Az adat resz elott nincs ':'

	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s %s Adat resz elol hianyzik a szeparalo \':\'.\r\n", 
			sender_nick,
			ERROR_COMMAND, 
			words[4],
			ERROR_CODE_DATA_NOSEPARATOR);
	    FIFO_add_item(&IRC_data_FIFO, 
			IRC_out_buffer, 
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen adatkuldest kezdemenyezett. (\':\' hianyzik)\n", sender_nick);
	
	    return FALSE;
	 }

	if (  !(temp_socket_item = find_socket( socket_list, sock_number, words[4], sender_nick) ))
	 {
//olyan nick ID kombinaciora probal irni, ami nem letezik

	    sprintf(IRC_out_buffer,
			    "PRIVMSG %s :%s %s %s Kapcsolat nincs kiepitve. Eloszor: \"%s ID cim port\"\r\n", 
			    sender_nick, 
			    ERROR_COMMAND,
			    words[4],
			    ERROR_CODE_DATA_NOCONNECTION,
			    OUT_COMMAND);
	    FIFO_add_item(&IRC_data_FIFO, 
			    IRC_out_buffer, 
			    strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen adatkuldest kezdemenyezett. (Kapcsolat (%s) nincs kiepitve)\n", sender_nick, words[4]);
	
	    return FALSE;
	 }

/*Data erkezett letezo bejegyzesnek*/	
	temp_string = get_word(command_buffer,
					6,
					&word_length,
					DEFAULT_WHITESPACE_CHARS,
					strlen(command_buffer) )
			+ 
			sizeof(char); //atugorjuk a bevezeto ':'-ot

	data = decode( temp_string, &word_length );

	FIFO_add_item(&(temp_socket_item->output_FIFO),
				data,
				word_length
				);
	free(data);
	
	printf( "*** %s %s reszere adat a kimenolistaban elhelyezve.\n", sender_nick, words[4]);
	
	return TRUE;
     }

    if ( !strcmp(words[3] + sizeof(char), OUT_COMMAND ) )     
     {
/*
words[0] -> sender_nick!username@host
words[1] -> PRIVMSG
words[2] -> reciever_nick
words[3] -> OUT_COMMAND
words[4] -> ID
words[5] -> host
words[6] -> port
words[7] -> NULL
words[8] -> NULL

*/

	if (word_counter < 7)
	 {
/*nincs kello szamu parameter*/
	    sprintf(IRC_out_buffer,

			"PRIVMSG %s :%s %s %s Kapcsolat nyitas helytelen. \"%s ID cim port\"\r\n", 
			sender_nick, 
			ERROR_COMMAND,
			words[4],
			ERROR_CODE_OUT_WRONGUSAGE,
			OUT_COMMAND);
	    FIFO_add_item(&IRC_data_FIFO, 
			IRC_out_buffer, 
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen kapcsolatnyitast kezdemenyezett. (Parameterszam nem megfelelo)\n", sender_nick);
	
	    return FALSE;
	 }

	if (strlen(words[4]) > ID_LENGTH)
	 {
//Az ID parameter tulsagosan hosszu
	    words[4][ID_LENGTH] = '\0';

	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s... %s %d lehet az ID maximalis hosszusaga.\r\n",
			sender_nick,
			ERROR_COMMAND,
			words[4],
			ERROR_CODE_OUT_IDTOOLONG,
    			ID_LENGTH);
	    FIFO_add_item(&IRC_data_FIFO,
			IRC_out_buffer,
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen kapcsolatnyitast kezdemenyezett. (ID tul hosszu)\n", sender_nick);
	
	    return FALSE;
	 }

	if ( !(i = strtoul(words[6], NULL, 10) ) )
	 {
/*port hibasan lett megadva*/	     
	    sprintf(IRC_out_buffer,
			    "PRIVMSG %s :%s %s %s A port (%s) hibasan lett megadva.\r\n", 
			    sender_nick,
			    ERROR_COMMAND, 
			    words[4],
			    ERROR_CODE_OUT_WRONGPORT,
			    words[6]);
	    FIFO_add_item(&IRC_data_FIFO, 
			    IRC_out_buffer, 
			    strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen kapcsolatnyitast kezdemenyezett. (port hibas)\n", sender_nick);
	
	    return FALSE;
	 }

	if (  find_socket( socket_list, sock_number, words[4], sender_nick) )
	 {
/*mar van ilyen felhasznalo ID kombinacio*/
	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s %s Az ID mar hasznalatban van.\r\n", 
		        sender_nick,
			ERROR_COMMAND, 
		        words[4],
			ERROR_CODE_OUT_NICKIDINUSE);
	    FIFO_add_item(&IRC_data_FIFO, 
			IRC_out_buffer, 
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen kapcsolatnyitast kezdemenyezett. (ID mar hasznalatban van)\n", sender_nick);
	
	    return FALSE;
	 }

	switch ( nagyon_atmeneti_valtozo = make_connection(words[5], i, &fd))
	 {
	    case -1:
	     {
/*A host(%s) hibasan lett megadva*/
		sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s A host(%s) hibasan lett megadva.\r\n", 
				    sender_nick,
				    ERROR_COMMAND, 
				    words[4],
				    ERROR_CODE_OUT_WRONGHOST,
				    words[5]);
		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s helytelen kapcsolatnyitast kezdemenyezett. (host hibas)\n", sender_nick);
	
		return FALSE;
	     }
	    case -2:
	     {
/*A socket nem sikerult kinyitni.*/
		sprintf(IRC_out_buffer,
    				    "PRIVMSG %s :%s %s %s Socket hiba(%s %s)\r\n", 
        			    sender_nick,
				    ERROR_COMMAND, 
    				    words[4],
				    ERROR_CODE_OUT_SOCKWONTOPEN,
    	    	    		    words[5],
    				    words[6]);
    		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s altal kert socketet nem sikerult letrehozni.\n", sender_nick);
	
		return FALSE;
	     }
    	    case -3:
	     {
/*A kapcsolatot nem lehetett kinyitni*/
		sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s Kapcsolodasi hiba.(%s %s)\r\n", 
				    sender_nick,
				    ERROR_COMMAND,
				    words[4],
				    ERROR_CODE_OUT_COULDNOTCONNECT,
				    words[5],
				    words[6]);
		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s altal kert kapcsolodas nem sikerult.\n", sender_nick);
	
		return FALSE;
	     }
	 }
	
/*A kapcsolat sikeresen letrehozva*/
	add_socket(&socket_list, 
		    &sock_number, 
		    words[4], 
		    fd, 
		    sender_nick, 
		    0, 
		    words[5],
		    i,
		    NULL);

	sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s Kapcsolat sikeresen letrehozva.(%s %s)\r\n", 
				    sender_nick,
				    REPORT_COMMAND, 
				    words[4],
				    REPORT_CODE_OUT_SUCCESS,
				    words[5],
				    words[6]);
	FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

	printf( "*** %s altal kert kapcsolodat sikeresen letrehozva.(%s %s)\n", sender_nick, words[5], words[6]);
	
	return TRUE;
     }

    if ( !strcmp(words[3] + sizeof(char), CLOSE_COMMAND ) )     
     {

/*
words[0] -> sender_nick!username@host
words[1] -> PRIVMSG
words[2] -> reciever_nick
words[3] -> CLOSE_COMMAND
words[4] -> ID
words[5] -> NULL
words[6] -> NULL
words[7] -> NULL
words[8] -> NULL

*/
	if (word_counter < 5)
	 {
/*nincs kello szamu parameter*/
	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s %s kapcsolat zaras helytelen. \"%s ID\"\r\n", 
			sender_nick,
			ERROR_COMMAND, 
			words[4],
			ERROR_CODE_CLOSE_WRONGUSAGE,
			CLOSE_COMMAND);
	    FIFO_add_item(&IRC_data_FIFO, 
			IRC_out_buffer, 
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen kapcsolatzarast kezdemenyezett. (Parameterszam nem megfelelo)\n", sender_nick);
	
	    return FALSE;
	 }

	if (strlen(words[4]) > ID_LENGTH)
	 {
//Az ID parameter tulsagosan hosszu
	    words[4][ID_LENGTH] = '\0';

	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s... %s %d lehet az ID maximalis hosszusaga.\r\n",
			sender_nick,
			ERROR_COMMAND,
			words[4],
			ERROR_CODE_CLOSE_IDTOOLONG,
    			ID_LENGTH);
	    FIFO_add_item(&IRC_data_FIFO,
			IRC_out_buffer,
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen kapcsolatzarast kezdemenyezett. (ID hossza nem megfelelo)\n", sender_nick);

	    return FALSE;
	 }

	if (  !(temp_socket_item = find_socket( socket_list, sock_number, words[4], sender_nick)) )
	 {
//olyan nick ID kombinaciot probal becsukni, ami nem letezik
	    sprintf(IRC_out_buffer,
			    "PRIVMSG %s :%s %s %s Kapcsolat nen szerepel a listaban.\r\n", 
			    sender_nick,
			    ERROR_COMMAND, 
			    words[4],
			    ERROR_CODE_CLOSE_NOCONNECTION);
	    FIFO_add_item(&IRC_data_FIFO, 
			    IRC_out_buffer, 
			    strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen kapcsolatzarast kezdemenyezett. ((%s) ID nincs a listaban)\n", sender_nick, words[4]);
	
	    return FALSE;
	 }
	
//kapcsolat sikeres zarasa	
	remove_socket( &socket_list, &sock_number, temp_socket_item );

	sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s A kapcsolat sikeresen zarva.\r\n", 
				    sender_nick,
				    REPORT_COMMAND, 
				    words[4],
				    REPORT_CODE_CLOSE_SUCCESS);
	FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

	printf( "*** %s altal kert kapcsolat sikeresen lezarva. (%s)\n", sender_nick, words[4]);

	return TRUE;
     }

    if ( !strcmp(words[3] + sizeof(char), ERROR_COMMAND ) )     
     {
/*
words[0] -> sender_nick!username@host
words[1] -> PRIVMSG
words[2] -> reciever_nick
words[3] -> ERROR_COMMAND
words[4] -> ID
words[5] -> ERROR_CODE
words[6] -> errormessage starts here...
words[7] -> 
words[8] -> 

*/
//Ha egy kapcsolattal barmilyen jellegu hiba merul fel, eltavolitja a program
//a listabol minden a kacsolattal kapcsolatos bejegyzest, es az IRC vonalon 
//nem reagal semmit: HIBARA VALASZT NEM KULD!
//KIVETEL: Ha tavoli kapcsolatot pusztan azert nem lehetett kiepiteni, mert 
//rossz ID-t valasztott, akkor uj ID-t general es ujraprobalkozik
	if (word_counter < 6)
	 {
//helytelen hasznalat
	    printf( "*** %s helytelen formatumu hibat kuldott. (Parameterszam nem megfelelo)\n", sender_nick);

	    return FALSE;
	 }

	if (strlen(words[4]) > ID_LENGTH)
	 {
//Az ID parameter tulsagosan hosszu
	    printf( "*** %s helytelen formatumu hibat kuldott. (ID tul hosszu)\n", sender_nick);
	
	    return FALSE;
	 }
//ha szerepel a helyi adatbazisban az uzenetkuldoje
	if ( !(temp_socket_item = find_socket( socket_list, sock_number, words[4], sender_nick) )) 
	 {
	    printf( "*** %s hibas hibat kuldott. ((%s) ID nem szerepel a listaban)\n", sender_nick, words[4]);
	
	    return FALSE;
	 }

	if ( !strcmp(words[5], ERROR_CODE_OUT_NICKIDINUSE ) )     
	 {
//ha szerepel a tavoli adatbazisban az ID, usernev kombinacio mas IDvel probalkozunk
	    while
	     ( 
		find_socket(socket_list, sock_number,  
		(ID = generate_random_string(ID_LENGTH, ID_LENGTH, DEFAULT_USABLE_CHARS) ), 
		    sender_nick)
	     ) free(ID);
	
	    strcpy(temp_socket_item->ID, ID);		
	 
/*
words[0] -> sender_nick!username@host
words[1] -> PRIVMSG
words[2] -> reciever_nick
words[3] -> ERROR_COMMAND
words[4] -> ID
words[5] -> ERROR_CODE
words[6] -> errormessage starts here...
words[7] -> 
words[8] -> 

*/
	    sprintf(IRC_out_buffer,
		    "PRIVMSG %s :%s %s %s %d\r\n", 
		    sender_nick,
		    OUT_COMMAND, 
		    ID,
		    temp_socket_item->host,
		    temp_socket_item->host_port
		    );
	    FIFO_add_item(&IRC_data_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));


	    printf(
		  	 "*** %s A generalt ID-m mar szerepel a tavoli adatbazisban. (%s) Probalkozom egy masikkal...\n", sender_nick, words[4]);

	    return TRUE;		 
	 }
			

	remove_socket(&socket_list, &sock_number, temp_socket_item);	
			
	if ( !strcmp(words[5], ERROR_CODE_OUT_WRONGHOST ) )     
	 {
//    "PRIVMSG %s :E %s 009 A host(%s) hibasan lett megadva.\r\n", 
	    printf( "*** %s A masik felnek problemaja van a halozati kapcsolattal, eltavolitom a listabol. (%s, hibas host)\n", sender_nick, words[4] );
	
	    return TRUE;				
	 }

	if ( !strcmp(words[5], ERROR_CODE_OUT_SOCKWONTOPEN ) )     
	 {
//    "PRIVMSG %s :E %s 010 A socket nem sikerult kinyitni(%s %s).\r\n", 
	    printf( "*** %s A masik felnek problemaja van a halozati kapcsolattal, eltavolitom a listabol. (%s, sock hiba)\n", sender_nick, words[4] );

	    return TRUE;
	 }
	if ( !strcmp(words[5], ERROR_CODE_OUT_COULDNOTCONNECT ) )     
	 {
//    "PRIVMSG %s :E %s 011 A kapcsolatot nem lehetett letrehozni(%s %s).\r\n", 
	    printf( "*** %s A masik felnek problemaja van a halozati kapcsolattal, eltavolitom a listabol. (%s, kapcsolodasi hiba)\n", sender_nick, words[4] );
	
	    return TRUE;
	 }

	printf(
		  "*** %s A masik felnek problemaja van a halozati kapcsolattal, eltavolitom a listabol. (%s)\n", sender_nick, words[4] );
	

	return FALSE;
     }

    if ( !strcmp(words[3] + sizeof(char), PILLERERROR_COMMAND ) )     
     {
/*
words[0] -> sender_nick!username@host
words[1] -> PRIVMSG
words[2] -> reciever_nick
words[3] -> PILLERERROR_COMMAND
words[4] -> ID
words[5] -> PILLERERROR_CODE
words[6] -> errormessage starts here...
words[7] -> 
words[8] -> 

*/

//Ha egy kapcsolattal barmilyen jellegu hiba merul fel, eltavolitja a program
//a pillerlistabol minden a kacsolattal kapcsolatos bejegyzest, es az IRC vonalon 
//nem reagal semmit: HIBARA VALASZT NEM KULD!
//KIVETEL: Ha tavoli kapcsolatot pusztan azert nem lehetett kiepiteni, mert 
//rossz ID-t valasztott, akkor uj ID-t general es ujraprobalkozik
	if (word_counter < 6)
	 {
//helytelen hasznalat
	    printf( "*** %s helytelen formatumu piller hibat kuldott. (Parameterszam nem megfelelo)\n", sender_nick);
	

	    return FALSE;
	 }

	if (strlen(words[4]) > ID_LENGTH)
	 {
//Az ID parameter tulsagosan hosszu
	    printf( "*** %s helytelen formatumu piller hibat kuldott. (ID tul hosszu)\n", sender_nick);
	
	    return FALSE;
	 }
//ha szerepel a helyi adatbazisban az uzenetkuldoje
	if ( !(temp_socket_item = find_socket( piller_socket_list, piller_sock_number, words[4], sender_nick) )) 
	 {
	    printf( "*** %s hibas hibat kuldott. ((%s) ID nem szerepel a listaban)\n", sender_nick, words[4]);

	    return FALSE;
	 }

	if ( !strcmp(words[5], PILLERERROR_CODE_PILLEROUT_NICKIDINUSE ) )     
	 {
//ha szerepel a tavoli adatbazisban az ID, usernev kombinacio mas IDvel probalkozunk
	    while
	     ( 
		find_socket(piller_socket_list, piller_sock_number,  
		(ID = generate_random_string(ID_LENGTH, ID_LENGTH, DEFAULT_USABLE_CHARS) ), 
		    sender_nick)
	     ) free(ID);
	
	    strcpy(temp_socket_item->ID, ID);		
	    strcpy((temp_socket_item->partner)->ID, ID);		
	 
	    sprintf(IRC_out_buffer,
		    "PRIVMSG %s :%s %s %s %d %s %d\r\n", 
		    sender_nick,
		    PILLEROUT_COMMAND, 
		    ID,
		    (temp_socket_item->partner)->host,
		    (temp_socket_item->partner)->port,
		    temp_socket_item->host,
		    temp_socket_item->port
		    );
	    FIFO_add_item(&IRC_data_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));


	    printf( "*** %s A generalt piller ID-m mar szerepel a tavoli adatbazisban. (%s) Probalkozom egy masikkal...\n", sender_nick, words[4]);
	
	    return TRUE;		 
	 }
			

	while ( remove_socket( &piller_socket_list, &piller_sock_number, find_socket( piller_socket_list, piller_sock_number, words[4], sender_nick ) ) ) { }
			
	if ( !strcmp(words[5], PILLERERROR_CODE_PILLEROUT_WRONGHOST ) )     
	 {
//    "PRIVMSG %s :E %s 009 A host(%s) hibasan lett megadva.\r\n", 
	    printf( "*** %s A masik piller felnek problemaja van a halozati kapcsolattal, eltavolitom a listabol. (%s, hibas host)\n", sender_nick, words[4] );
	
	    return TRUE;				
	 }

	if ( !strcmp(words[5], ERROR_CODE_OUT_SOCKWONTOPEN ) )     
	 {
//    "PRIVMSG %s :E %s 010 A socket nem sikerult kinyitni(%s %s).\r\n", 
	    printf( "*** %s A masik piller felnek problemaja van a halozati kapcsolattal, eltavolitom a listabol. (%s, sock hiba)\n", sender_nick, words[4] );
	
	    return TRUE;
	 }
	if ( !strcmp(words[5], ERROR_CODE_OUT_COULDNOTCONNECT ) )     
	 {
//    "PRIVMSG %s :E %s 011 A kapcsolatot nem lehetett letrehozni(%s %s).\r\n", 
	    printf( "*** %s A masik piller felnek problemaja van a halozati kapcsolattal, eltavolitom a listabol. (%s, kapcsolodasi hiba)\n", sender_nick, words[4] );
	
	    return TRUE;
	 }

	printf( "*** %s A masik piller felnek problemaja van a halozati kapcsolattal, eltavolitom a listabol. (%s)\n", sender_nick, words[4] );
	
	return FALSE;
     }


#ifdef DEBUG
printf("\nR feltetel vizsgalata elott...\n");
#endif

    if ( !strcmp(words[3] + sizeof(char), REPORT_COMMAND ) )     
     {
#ifdef DEBUG
printf("R feltetel vizsgalata utan, 001 elott\n");
#endif
/*
words[0] -> sender_nick!username@host
words[1] -> PRIVMSG
words[2] -> reciever_nick
words[3] -> REPORT_COMMAND
words[4] -> ID
words[5] -> REPORT_CODE
words[6] -> message starts here...
words[7] -> 
words[8] -> 

*/
	if ( !strcmp(words[5], REPORT_CODE_OUT_SUCCESS ) )     
	 {
#ifdef DEBUG
printf("001 utan...\n");
#endif
//ha van a sikeres kapcsolatot kiepito userrol bejegyzes
	    if ( !(temp_socket_item = find_socket( socket_list, sock_number, words[4], sender_nick) ))
	     {	
		sprintf(IRC_out_buffer,
			    "PRIVMSG %s :%s %s %s ID nem szerepel a listaban.\r\n", 
			    sender_nick,
			    ERROR_COMMAND, 
			    words[4],
			    ERROR_CODE_REPORT_NOCONNECTION);
		FIFO_add_item(&IRC_data_FIFO, 
			    IRC_out_buffer, 
			    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s Altal kuldott jelentes hibas. ((%s) ID nincs a listaban)\n", sender_nick, words[4]);

		return FALSE;
	     }
//tavoli kapcsolat osszejott
	    printf( "*** %s A tavoli kapcsolat sikeresen megnyilt. (%s)\n", sender_nick, words[4]);
	
	    if ( 
		(
		    temp_socket_item->fd 
		    = 
		    make_connection_server(temp_socket_item->port)
		) 
		<= 
		0 
		)	
	     {

#ifdef DEBUG2
printf("listen socketet megprobaltam megnyitni\n");
#endif
		switch ( temp_socket_item->fd  )
		 {
		    case -1:
			sprintf(IRC_out_buffer,
    				    "PRIVMSG %s :%s %s %s Szerver socket hiba, listabol eltavolitva.\r\n", 
        			    sender_nick,
				    ERROR_COMMAND, 
    				    words[4],
				    ERROR_CODE_REPORT_SOCKWONTOPEN);
    			FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));


			printf( "*** %s Helyi szerver hiba. ((%s) socket)\n", sender_nick, words[4] );
	
			break;
		    case -2:

			sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s Szerver bind hiba, listabol eltavolitva..\r\n", 
				    sender_nick,
				    ERROR_COMMAND, 
				    words[4],
				    ERROR_CODE_REPORT_COULDNOTBIND);
			FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

			printf( "*** %s Helyi szerver hiba. ((%s) bind)\n", sender_nick, words[4] );
	
			break;
		    case -3:


/*A kapcsolatot nem lehetett kinyitni*/
			sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s Szerver listen hiba, listabol eltavolitva..\r\n", 
				    sender_nick,
				    ERROR_COMMAND,
				    words[4],
				    ERROR_CODE_REPORT_COULDNOTLISTEN);
			FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

			printf( "*** %s Helyi szerver hiba. ((%s) listen)\n", sender_nick, words[4] );
	
			break;
		 }
					
		remove_socket(&socket_list, &sock_number, temp_socket_item);
		
		return FALSE; 
	     }	 					

	    printf( "*** %s A helyi kapcsolat sikeresen megnyilt, varakozik. (%s)\n", sender_nick, words[4] );
	
	    return TRUE;

	 }
	return FALSE;	    
     }
     
    if ( !strcmp(words[3] + sizeof(char), PILLERREPORT_COMMAND ) )     
     {
#ifdef DEBUG2
printf("R feltetel vizsgalata utan, 001 elott\n");
#endif
/*
words[0] -> sender_nick!username@host
words[1] -> PRIVMSG
words[2] -> reciever_nick
words[3] -> PILLERREPORT_COMMAND
words[4] -> ID
words[5] -> PILLERREPORT_CODE
words[6] -> message starts here...
words[7] -> 
words[8] -> 

*/

	if ( !strcmp(words[5], PILLERREPORT_CODE_PILLEROUT_SUCCESS ) )     
	 {
#ifdef DEBUG2
printf("command_buffer: %s\n", command_buffer);
#endif
	 
//ha van a sikeres kapcsolatot kiepito userrol bejegyzes
	    if ( !(temp_socket_item = find_socket( piller_socket_list, piller_sock_number, words[4], sender_nick) ))
	     {	
		sprintf(IRC_out_buffer,
			    "PRIVMSG %s :%s %s %s piller ID nem szerepel a listaban.\r\n", 
			    sender_nick,
			    PILLERERROR_COMMAND, 
			    words[4],
			    PILLERERROR_CODE_PILLERREPORT_NOCONNECTION);
		FIFO_add_item(&IRC_data_FIFO, 
			    IRC_out_buffer, 
			    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s Altal kuldott piller jelentes hibas. ((%s) ID nincs a listaban)\n", sender_nick, words[4]);

		return FALSE;
	     }
//tavoli kapcsolat osszejott
	    printf( "*** %s A tavoli piller kapcsolat sikeresen megnyilt. (%s)\n", sender_nick, words[4]);
	
	    return TRUE;

	 }
	return FALSE;	    
     }
    if ( !strcmp(words[3] + sizeof(char), PILLEROUT_COMMAND ) )     
     {

#ifdef DEBUG2
printf("9 szo.....\n");
#endif

	if (word_counter < 9)
	 {
/*nincs kello szamu parameter*/

	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s %s Piller kapcsolat nyitas helytelen. \"%s ID helyi_cim port cim port\"\r\n", 
			sender_nick, 
			PILLERERROR_COMMAND,
			words[4],
			PILLERERROR_CODE_PILLEROUT_WRONGUSAGE,
			PILLEROUT_COMMAND);
	    FIFO_add_item(&IRC_data_FIFO, 
			IRC_out_buffer, 
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen piller kapcsolatnyitast kezdemenyezett. (Parameterszam nem megfelelo)\n", sender_nick);
	
	    return FALSE;
	 }

#ifdef DEBUG2
printf("9 szo.....\n");
#endif
	if (strlen(words[4]) > ID_LENGTH)
	 {
//Az ID parameter tulsagosan hosszu
	    words[4][ID_LENGTH] = '\0';

	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s... %s %d lehet a piller ID maximalis hosszusaga.\r\n",
			sender_nick,
			PILLERERROR_COMMAND,
			words[4],
			PILLERERROR_CODE_PILLEROUT_IDTOOLONG,
    			ID_LENGTH);
	    FIFO_add_item(&IRC_data_FIFO,
			IRC_out_buffer,
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen piller kapcsolatnyitast kezdemenyezett. (ID tul hosszu)\n", sender_nick);
	
	    return FALSE;
	 }

#ifdef DEBUG2
printf("megfelelo ID length.....\n");
#endif
	if ( !(i = strtoul(words[6], NULL, 10) ) || !(strtoul(words[8], NULL, 10) ) )
	 {
/*port hibasan lett megadva*/	     
	    sprintf(IRC_out_buffer,
			    "PRIVMSG %s :%s %s %s A piller portja hibasan lett megadva.\r\n", 
			    sender_nick,
			    PILLERERROR_COMMAND, 
			    words[4],
			    PILLERERROR_CODE_PILLEROUT_WRONGPORT);
	    FIFO_add_item(&IRC_data_FIFO, 
			    IRC_out_buffer, 
			    strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen piller kapcsolatnyitast kezdemenyezett. (port hibas)\n", sender_nick);
	
	    return FALSE;
	 }
#ifdef DEBUG2
printf("portok is jok.\n");
#endif


	if (  find_socket(piller_socket_list, piller_sock_number,  words[4], sender_nick) )
	 {
//mar van ilyen felhasznalo ID kombinacio
	    sprintf(IRC_out_buffer,
			"PRIVMSG %s :%s %s %s Az piller ID mar hasznalatban van.\r\n", 
		        sender_nick,
			PILLERERROR_COMMAND, 
		        words[4],
			PILLERERROR_CODE_PILLEROUT_NICKIDINUSE);
	    FIFO_add_item(&IRC_data_FIFO, 
			IRC_out_buffer, 
			strlen(IRC_out_buffer) * sizeof(char));

	    printf( "*** %s helytelen piller kapcsolatnyitast kezdemenyezett. (ID mar hasznalatban van)\n", sender_nick);
	
	    return FALSE;
	 }
#ifdef DEBUG2
printf("nem is szerepel ez az ID a listaban\n");
#endif

#ifdef DEBUG2
printf("probalom kiepiteni a kapcsolatot: %s %d\n",words[5], i);
#endif

	switch ( nagyon_atmeneti_valtozo = make_connection(words[5], i, &fd))
	 {
	    case -1:
	     {
/*A host(%s) hibasan lett megadva*/
		sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s A piller host(%s) hibasan lett megadva.(C)\r\n", 
				    sender_nick,
				    PILLERERROR_COMMAND, 
				    words[4],
				    PILLERERROR_CODE_PILLEROUT_CWRONGHOST,
				    words[5]);
		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s helytelen piller kapcsolatnyitast kezdemenyezett. (C host hibas)\n", sender_nick);
	
		return FALSE;
	     }
	    case -2:
	     {
/*A socket nem sikerult kinyitni.*/
		sprintf(IRC_out_buffer,
    				    "PRIVMSG %s :%s %s %s Piller socket hiba(C %s %s)\r\n", 
        			    sender_nick,
				    PILLERERROR_COMMAND, 
    				    words[4],
				    PILLERERROR_CODE_PILLEROUT_CSOCKWONTOPEN,
    	    	    		    words[5],
    				    words[6]);
    		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s altal kert piller socketet nem sikerult letrehozni.(C)\n", sender_nick);
	
		return FALSE;
	     }
    	    case -3:
	     {
/*A kapcsolatot nem lehetett kinyitni*/
		sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s Piller kapcsolodasi hiba.(C %s %s)\r\n", 
				    sender_nick,
				    PILLERERROR_COMMAND,
				    words[4],
				    PILLERERROR_CODE_PILLEROUT_CCOULDNOTCONNECT,
				    words[5],
				    words[6]);
		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s altal kert piller kapcsolodas nem sikerult.(C)\n", sender_nick);
	
		return FALSE;
	     }
	 }

#ifdef DEBUG2
printf("sikerult kiepiteni a kapcsolatot: %s %d\n",words[5], i);
#endif

	
/*A kapcsolat sikeresen letrehozva*/
	add_socket(&piller_socket_list, 
		    &piller_sock_number, 
		    words[4], 
		    fd, 
		    sender_nick, 
		    0,
		    words[5],
		    i,
		    NULL);
#ifdef DEBUG2
printf("hozzadaom a kov bejegyzest a listahoz:ID %s\n\tfd %d\n\tuser %s\n\thelyiport %d\n\ttavolihost %s\n\thostport %d\n",
		    words[4], 
		    fd, 
		    sender_nick, 
		    0,
		    words[5],
		    i
		);
#endif


	sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s Piller kapcsolat sikeresen letrehozva.(C %s %s)\r\n", 
				    sender_nick,
				    PILLERREPORT_COMMAND, 
				    words[4],
				    PILLERREPORT_CODE_PILLEROUT_SUCCESS,
				    words[5],
				    words[6]);

#ifdef DEBUG2
printf("kuldom IRCn: %s\n",IRC_out_buffer);
#endif


	FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

	printf( "*** %s altal kert piller kapcsolatot sikeresen letrehozva.(C %s %s)\n", sender_nick, words[5], words[6]);
	
	i = strtoul(words[8], NULL, 10);

#ifdef DEBUG2
printf("probalom kiepiteni a kapcsolatot: %s %d\n",words[7], i);
#endif


	switch ( nagyon_atmeneti_valtozo =  make_connection(words[7], i, &fd))
	 {
	    case -1:
	     {
/*A host(%s) hibasan lett megadva*/
		sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s A piller host(%s) hibasan lett megadva.(D)\r\n", 
				    sender_nick,
				    PILLERERROR_COMMAND, 
				    words[4],
				    PILLERERROR_CODE_PILLEROUT_DWRONGHOST,
				    words[5]);
		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s helytelen piller kapcsolatnyitast kezdemenyezett. (D host hibas)\n", sender_nick);
	
		return FALSE;
	     }
	    case -2:
	     {
/*A socket nem sikerult kinyitni.*/
		sprintf(IRC_out_buffer,
    				    "PRIVMSG %s :%s %s %s Piller socket hiba(D %s %s)\r\n", 
        			    sender_nick,
				    PILLERERROR_COMMAND, 
    				    words[4],
				    PILLERERROR_CODE_PILLEROUT_DSOCKWONTOPEN,
    	    	    		    words[5],
    				    words[6]);
    		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s altal kert piller socketet nem sikerult letrehozni.(D)\n", sender_nick);
	
		return FALSE;
	     }
    	    case -3:
	     {
/*A kapcsolatot nem lehetett kinyitni*/
		sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s Piller kapcsolodasi hiba.(D %s %s)\r\n", 
				    sender_nick,
				    PILLERERROR_COMMAND,
				    words[4],
				    PILLERERROR_CODE_PILLEROUT_DCOULDNOTCONNECT,
				    words[5],
				    words[6]);
		FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** %s altal kert piller kapcsolodas nem sikerult.(D)\n", sender_nick);
	
		return FALSE;
	     }
	 }

#ifdef DEBUG2
printf("sikerult kiepiteni a kapcsolatot: %s %d %d\n",words[5], i, fd);
#endif

	temp_socket_item = piller_socket_list;

	add_socket(&piller_socket_list, 
		&piller_sock_number, 
		words[4], 
		fd, 
		sender_nick, 
		0,
		words[7],
		i, 
		temp_socket_item);

	temp_socket_item->partner = piller_socket_list;			    

#ifdef DEBUG2
printf("hozzadaom a kov bejegyzest a listahoz:ID %s\n\tfd %d\n\tuser %s\n\thelyiport %d\n\ttavolihost %s\n\thostport %d\n",
		words[4], 
		fd, 
		sender_nick, 
		0,
		words[7],
		i
		);
#endif

	
	sprintf(IRC_out_buffer,
				    "PRIVMSG %s :%s %s %s Piller kapcsolat sikeresen letrehozva.(D %s %s)\r\n", 
				    sender_nick,
				    PILLERREPORT_COMMAND, 
				    words[4],
				    PILLERREPORT_CODE_PILLEROUT_SUCCESS,
				    words[7],
				    words[8]);
	FIFO_add_item(&IRC_data_FIFO, 
				    IRC_out_buffer, 
				    strlen(IRC_out_buffer) * sizeof(char));

#ifdef DEBUG2
printf("kuldom IRCn: %s\n",IRC_out_buffer);
#endif


	printf( "*** %s altal kert piller kapcsolodat sikeresen letrehozva.(D %s %s)\n", sender_nick, words[7], words[8]);
	
	return TRUE;
     }


     return FALSE;
 }


boolean IRC_command_processing(char* command_buffer)
 {
    unsigned short word_length; 
    unsigned short word_counter = 0;
    unsigned short i = 0;
    char* words[9];
    boolean out;
    char* temp_string;	

//get words from 1 to 9
//elso het szo gyujtese
    for (i=0; i < 9; i++) words[i] = NULL;

    while 
     ( 
         (word_counter < 9)
	&& 
	 (
	    temp_string = get_word(
		command_buffer,
		word_counter+1,
		&word_length,
		DEFAULT_WHITESPACE_CHARS,
		strlen(command_buffer) )
	 )   
     )
     {
	words[word_counter] = (char *) malloc( sizeof(char) * (word_length + 1) );
	strncpy(words[word_counter], temp_string, word_length);
	words[word_counter][word_length] = '\0';

	word_counter++;
     }

//Got no words? Get ta hell outta here!
//Ha nincsenek szavak, akkor vege 
    if (! word_counter ) return TRUE;

    out = IRC_command_interpretation(words, command_buffer, word_counter);

    for (i=0; i < word_counter; i++) free(words[i]);

    return out;
 }


void list_conf()
 {
	
    printf( 
	    "*** Konfiguraciok listaja:\n");
	

    printf(    "\tserver: %s\n", IRC_server);

    printf( 
	    "\tport: %d\n", IRC_port);
	
    printf(    "\tnick: %s\n", IRC_nick);


    printf(    "\tother_nick: %s\n", IRC_other_nick);

    printf(    "\treal_name: %s\n", IRC_real_name);

    printf(    "\tchunk_size: %d\n", chunk_size);

    printf(    "\treconnect: %d\n", reconnect);

    printf(    "\treconnections: %d\n", reconnections);

    printf(    "\tlimit_reconnections: %d\n", limit_reconnections);

    printf(    "\tcommand_char: %c\n", command_char);
    
    printf(    "\tdelay: %d\n", delay);

    printf(    "\tverbose: %d\n", verbose);


/*	printf( 
	    "*** Piller socketlista vege.\n" );
	
*/
 }


int stdin_command_interpretation(char* words[6], char* command_buffer, unsigned short word_counter)
 {
    char ID[ID_LENGTH + 1];
    int i,fd;
    socket_list_item_type* temp_item;


//Got no words? Get ta hell outta here!
//ha nem kaptunk szavakat kilepunk
    if (! word_counter ) return 0;
    if ( verbose && (words[0][0] != command_char )) return 0;

    if ( !(strcmp(words[0] + (sizeof(char) * (boolean) verbose), TUNNEL_COMMAND )) )
     {	
	if ( word_counter != 5 )
	 {	
	    printf( "*** Helytelen hasznalat! /%s local_port nick host port\n", TUNNEL_COMMAND   );
	
	    return -1;	
	 }

	if ( !(strtoul(words[1], NULL, 10) ) || !(strtoul(words[4], NULL, 10) ) )
	 {
	    printf( "*** A portok nem szamok!\n"   );
	
	    return -1;
	 }

	while
	 ( 
	    find_socket(socket_list, sock_number,  
		    strcpy(ID, generate_random_string(ID_LENGTH, ID_LENGTH, DEFAULT_USABLE_CHARS) )
		    , 
		    words[2]
		)
	 );
	
    	add_socket(&socket_list, 
		    &sock_number, 
		    ID, 
		    0, 
		    words[2], 
		    strtoul(words[1], NULL, 10),
		    words[3],
		    strtoul(words[4], NULL, 10),
		    NULL);
	
	sprintf(IRC_out_buffer,
		    "PRIVMSG %s :%s %s %s %s\r\n", 
		    words[2],
		    OUT_COMMAND, 
		    ID,
		    words[3],
		    words[4]
		    );
	FIFO_add_item(&IRC_data_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

	printf( "*** Kapcsolatkerelem sikeresen elkuldve!\n"   );
	
	return 1;
     }

#ifdef DEBUG2
printf("lista elott\n");
#endif

    if ( !(strcmp(words[0] + (sizeof(char) * (boolean) verbose), CONF_LIST_COMMAND )) )
     {	
	list_conf();
	
/*	printf( 
	    "*** Piller socketlista vege.\n" );
	
*/
	return 1;
     }

    if ( !(strcmp(words[0] + (sizeof(char) * (boolean) verbose), LIST_COMMAND )) )
     {	
#ifdef DEBUG2
printf("listaban\n");
#endif
	
	temp_item = socket_list;

	printf( 
	    "*** Socketlista kezdodik:\n" );
	
	for (i=0; i<sock_number; i++)
	 {
	    printf( 
	    "%d. socket:\n\tID: %s\n\tnick: %s\n\tfd: %d\n\tport: %d\n",
	    i+1,
	    temp_item->ID,
	    temp_item->nick,
	    temp_item->fd,
	    temp_item->port
	     );
	
	    temp_item = temp_item->next;	 
	 }
	printf( 
	    "*** Socketlista vege.\n" );
	
	temp_item = piller_socket_list;

	printf( 
	    "*** Piller socketlista kezdodik:\n" );
	
	for (i=0; i<piller_sock_number; i++)
	 {
	    printf( 
	    "%d. socket:\n\tID: %s\n\tnick: %s\n\tfd: %d\n\tport: %d\n",
	    i+1,
	    temp_item->ID,
	    temp_item->nick,
	    temp_item->fd,
	    temp_item->port
	     );
	
	    temp_item = temp_item->next;	 
	 }
	printf( 
	    "*** Piller socketlista vege.\n" );
	
	return 1;
     }

    if ( !(strcmp(words[0] + (sizeof(char) * (boolean) verbose), DELAY_COMMAND )) )
     {	
	
	if ( word_counter != 2 )
	 {	
	    printf( "*** Helytelen hasznalat! /%s masodperc\n", DELAY_COMMAND );
	
	    return -1;	
	 }

	i = strtoul(words[1], NULL, 10); 
	if ( (i > 10) || (i < 0)  )
	{
	    printf( "*** Az ido 0 es 10 kozott vehet fel ertekeket.\n"   );
	
	    return -1;
	}


	delay = i;

	printf( 
	    "*** Delay sikeresen atalitva.\n" );
	
	return 1;
     }

    if ( !(strcmp(words[0] + (sizeof(char) * (boolean) verbose), VERBOSE_COMMAND )) )
     {	
	
	if ( word_counter != 2 )
	 {	
	    printf( "*** Helytelen hasznalat! (Ket szint van: 0 es 1): /%s szint\n", VERBOSE_COMMAND );
	
	    return -1;	
	 }

	i = strtoul(words[1], NULL, 10); 
	if ( (i > 1) || (i < 0)  )
	{
	    printf( "*** A szint ido 0 vagy 1 lehet.\n"   );
	
	    return -1;
	}


	verbose = i;

	printf( 
	    "*** Verbose sikeresen atalitva.\n" );
	
	return 1;
     }
     
    if ( !(strcmp(words[0] + (sizeof(char) * (boolean) verbose), EXTERNAL_TUNNEL_COMMAND )) )
     {	
#ifdef DEBUG2
printf("tunnel parancs ertelmezes...\n");
#endif

	if ( word_counter != 6 )
	 {	
	    printf( "*** Helytelen hasznalat! /%s local_host local_port nick host port\n", EXTERNAL_TUNNEL_COMMAND   );
	
	    return -1;	
	 }
#ifdef DEBUG2
printf("6 szavunk van...\n");
#endif


	if ( !(i = strtoul(words[2], NULL, 10) ) || !(strtoul(words[5], NULL, 10) ) )
	 {
	    printf( "*** A portok nem szamok!\n"   );
	
	    return -1;
	 }

#ifdef DEBUG2
printf("a portok szamok\n");
#endif


	while
	 ( 
	    find_socket(piller_socket_list, piller_sock_number,  
		    strcpy(ID, generate_random_string(ID_LENGTH, ID_LENGTH, DEFAULT_USABLE_CHARS) )
		    , 
		    words[3]
		)
	 );

#ifdef DEBUG2
printf("talaltam nem hasznalt ID-t: %s\n", ID);
#endif


	if ( 
	    (
		    fd 
		    = 
		    make_connection_server(i)
	    ) 
	    <= 
	    0 
	    )	
	 {

#ifdef DEBUG2
printf("probalok kapcsolatot epiteni, de nem sikerult, a kov eredmennyel: %d\n", fd);
#endif
		switch ( fd )
		 {
		    case -1:
			printf( "*** Helyi szerver hiba. ((A) socket)\n" );
	
			break;
		    case -2:

			printf( "*** Helyi szerver hiba. ((A) bind)\n");
	
			break;
		    case -3:
/*A kapcsolatot nem lehetett kinyitni*/
			printf( "*** Helyi szerver hiba. ((A) listen)\n");
	
			break;
		 }
					
		
		return -1; 
	     }	 					

#ifdef DEBUG2
printf("sikeresen kapcsolat! %d\n", fd);
#endif

	    printf( "*** A helyi szerver sikeresen megnyilt, varakozik. (A)\n");
	
	    add_socket(&piller_socket_list, 
			&piller_sock_number, 
			ID, 
			fd, 
			words[3], 
			strtoul(words[2], NULL, 10), 
			words[4],
			strtoul(words[5], NULL, 10),
			NULL);

#ifdef DEBUG2
printf("hozzadaom a kov bejegyzest a listahoz:ID %s\n\tfd %d\n\tuser %s\n\thelyiport %d\n\ttavolihost %s\n\thostport %d\n",
			ID, 
			fd, 
			words[3], 
			strtoul(words[2], NULL, 10), 
			words[4],
			strtoul(words[5], NULL, 10)
		);
#endif


#ifdef DEBUG2
printf("probalgatom a portokat\n");
#endif

	for (i=49152;
	    i < 65536
	    && 
	    (
		(
		    fd 
		    = 
		    make_connection_server(i)
		) 
		== 
		-2
	    );
	    i++ 
	    )	
	 {
	    printf( "*** Helyi szerver hiba. Ujra probalom... ((B) bind)\n");
	
	 }

#ifdef DEBUG2
printf("van port: %d\n", i);
#endif
#ifdef DEBUG2
printf("vizsgalom a kapcsolatokat %d\n", fd);
#endif

	if (fd <=0 )
	 {
		switch ( fd )
		 {
		    case -1:
			printf( "*** Helyi szerver hiba. ((B) socket)\n" );
	
			break;
		    case -2:
			printf( "*** Helyi szerver hiba. ((B) bind)\n");
	
			break;
		    case -3:
/*A kapcsolatot nem lehetett kinyitni*/
			printf( "*** Helyi szerver hiba. ((B) listen)\n");
	
			break;
		 }
					
		remove_socket(&piller_socket_list, &piller_sock_number, temp_item);
		return -1; 
	     }	 					

#ifdef DEBUG2
printf("minden rendben tunik!\n");
#endif

	    temp_item = piller_socket_list;

	    add_socket(&piller_socket_list, 
		    &piller_sock_number, 
		    ID, 
		    fd, 
		    words[3], 
		    i,
		    words[4],
		    strtoul(words[5], NULL, 10),
		    temp_item);
	    temp_item->partner = piller_socket_list;			    
#ifdef DEBUG2
printf("hozzadaom a kov bejegyzest a listahoz:ID %s\n\tfd %d\n\tuser %s\n\thelyiport %d\n\ttavolihost %s\n\thostport %d\n",
		    ID, 
		    fd, 
		    words[3], 
		    i,
		    words[4],
		    strtoul(words[5], NULL, 10)
		);
#endif
	sprintf(IRC_out_buffer,
		    "PRIVMSG %s :%s %s %s %d %s %s\r\n", 
		    words[3],
		    PILLEROUT_COMMAND, 
		    ID,
		    words[1],
		    i,
		    words[4],
		    words[5]
		    );
	FIFO_add_item(&IRC_data_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

	printf( "*** A piller kapcsolatkerelem sikeresen elkuldve.\n"   );
	
#ifdef DEBUG2
printf("vegul elkuldom IRCn:%s\n", IRC_out_buffer);
#endif
	return 1;
     }

    if ( !(strcmp(words[0] + (sizeof(char) * (boolean) verbose), QUIT_COMMAND )) )
     {	
	work = FALSE;

	printf( "*** A program kilep...\n"   );
	
	return 1;
     }
     
    if ( !(strcmp(words[0] + (sizeof(char) * (boolean) verbose), SERVER_COMMAND )) )
     {	
	if ( word_counter != 3 )
	 {	
	    printf( "*** Helytelen hasznalat! /%s IRC_server_host IRC_server_port\n", SERVER_COMMAND   );
	
	    return -1;	
	 }

	if ( !(strtoul(words[2], NULL, 10) ) )
	 {
	    printf( "*** A port nem szam!\n"   );
	
	    return -1;
	 }

	if (IRC_sock_fd > 0)
	 {
	    close(IRC_sock_fd);

	    printf( "*** IRC kapcsolat zarult.\n" );
	
	 }

	strcpy(IRC_server, words[1]);
	IRC_port = strtoul(words[2], NULL, 10);
	    
	IRC_sock_fd = -1;
	make_IRC_connection = TRUE;
	data_tr_enabled = FALSE;
	connection_counter = reconnections;    
#ifdef DEBUG3
printf("%d %d", connection_counter, make_IRC_connection);
#endif
	
	while (IRC_controller_FIFO) FIFO_del_first_item(&IRC_controller_FIFO);

	
	return 1;
     }

    printf( "*** Ismeretlen parancs\n" );
	
    return -1;

 }


int stdin_command_processing(char* command_buffer)
 {
    unsigned short i = 0;
    unsigned short word_length; 
    char* words[6];
    char* temp_string;
    unsigned short word_counter = 0;
    int out; 


/*get words from 1 to 6*/
    for (i=0; i < 6; i++) words[i] = NULL;

    while ( 
	    (word_counter < 6)
	    && 
    	    (
	    temp_string = get_word(
		command_buffer,
		word_counter+1,
		&word_length,
		DEFAULT_WHITESPACE_CHARS,
		strlen(command_buffer) 
		)
	    )   
	)
     {
        words[word_counter] = (char *) malloc( sizeof(char) * (word_length + 1) );
	strncpy(words[word_counter], temp_string, word_length);
	words[word_counter][word_length] = '\0';
	
	word_counter++;
     }

//Got no words? Get ta hell outta here!
//Akkor folytatjuk ha vannak kiertekelheto szavak
    if (! word_counter ) return 0;
    if (verbose && (words[0][0] != command_char)) return 0;
    
    out = stdin_command_interpretation(words, command_buffer, word_counter);

    for (i=0; i < word_counter; i++) free(words[i]);

    return out;
 }



boolean IRC_input()
 {
    int byte_counter;

/*    #ifdef DEBUG
    printf("\r\nIRC_input entered\r\n");
    #endif*/
    while (1)
     {
	if 
	 ( 
	    (byte_counter = read(IRC_sock_fd, &temp_char, sizeof(char) ) )
	    <=
	    0
	 )
	 {
	    if (
		!byte_counter
		||
		(
		    byte_counter < 0
		    &&    
		    errno != EAGAIN
		)
	    ) 
	     {
		/*
		PANIC!!!!
		TODO: Az IRC socket bezarult olvasas kozben!
*/
		return FALSE;
	     }
	    
	    return TRUE;
	 }

	if (verbose) putchar(temp_char); /*kiirom*/
	
	if (num_state == 0)
	 {
	    if ( (temp_char != '\r') && (temp_char != '\n') ) 
	     {
	    
		memset(IRC_in_buffer, 255, sizeof(char)*513);
		IRC_in_buffer[0] = '\0';
		IRC_in_buffer[513] = '\0';
		IRC_in_buffer_pointer = IRC_in_buffer;
	    
		collect(&IRC_in_buffer_pointer, temp_char, "");
		num_state = 1;
	     }
	 }
	else
	 {
	    if ( (temp_char != '\r') && (temp_char != '\n') ) 
	     {
		collect(&IRC_in_buffer_pointer, temp_char, "");
	     }	
	    else
	     {
		IRC_command_processing(IRC_in_buffer);
		num_state = 0;
	     }
	 }
     }
    return TRUE;
 }
 

void sock_input()
 {
    int i,j, byte_counter, client_socket; /*i szamolja a socketeket, j szamolja az olvasott karaktereket az i. socketen*/
    boolean can_read = TRUE, socket_closed = FALSE;
    char buffer[chunk_size + 2]; //<--- 1 byte jatek es 1 byte a lezarasnak

	
    for (i=0; i < sock_number; i++)
     {
	if ( socket_list->fd && FD_ISSET(socket_list->fd, &readfs))
	 {
	    if (socket_list->port)
	     {
		client_socket = accept(socket_list->fd, NULL, NULL);
		fcntl(client_socket, F_SETFL, (O_NDELAY | fcntl(client_socket,F_GETFL)) );
		close(socket_list->fd);
		socket_list->fd = client_socket;
		socket_list->port = 0;

		printf(
			"*** %s Tunnel kiepitve. (%s)\n", socket_list->nick, socket_list->ID);
	
	     }
	    else if (can_read)
	     {
		time(&s_time);	
	     
		for (j=0; j < chunk_size; j++)
		 {

		    if 
		        ( 
			    (byte_counter = read(socket_list->fd, &temp_char, sizeof(char) ))
			    <=
			    0
		        )
		     {

			if (
				!byte_counter
				||
				(
				    (byte_counter < 0)	
				    &&
				    errno != EAGAIN
				)
			    ) socket_closed = TRUE;
			    /*
			    (Socket bezarult olvasas kozben, esemeny
			    kezelese kesobb, elotte 
			    a mar megkapott adatokat azert elkuldjuk.)*/

			    break;
		     }

		    switch (temp_char)
		     {
			case '\n':
			 {
				buffer[j] = '%';
				j++;
				buffer[j] = 'n';
				break;
			 }

			case '\r':
			 {							    
				buffer[j] = '%';
				j++;
				buffer[j] = 'r';
				break;
			 }

			case '\0':
			 {
				buffer[j] = '%';
				j++;
				buffer[j] = '0';
				break;
			 }

			case '%':
			 {
				buffer[j] = '%';
				j++;
				buffer[j] = '%';
				break;
			 }
						    
			default:
				buffer[j] = temp_char;
		     }		
		 }

		buffer[j] = '\0';				    
/* ezen a ponton j tartalmazza a kodolt bitfolyam meretet
buffer tartalmazza a kodolt bitfolyamot
ezt hozza kell adni az irc kimeno bufferhez				    

*/	
		if ( j ) /*ha egyaltalan olvastam valamit*/
		 {

		    sprintf(IRC_out_buffer,
			    "PRIVMSG %s :%s %s :%s\r\n",
			    socket_list->nick, 
			    DATA_COMMAND, 
			    socket_list->ID,
			    buffer);
		    FIFO_add_item(&IRC_data_FIFO, 
			    IRC_out_buffer, 
			    strlen(IRC_out_buffer) * sizeof(char));
			    
		    printf( "*** Adat kuldve IRC halozaton.\n" );
	
		 }
	     }
	 }
	    
	if (socket_closed)
	 {
	    socket_closed = FALSE;
		/*
		Socket bezarult olvasas kozben! (A mar meglevo adatokat
		mar elkuldtuk.)*/

/*figyelmeztetes IRC-en*/
	    sprintf(IRC_out_buffer,
		    "PRIVMSG %s :%s %s %s A socket olvasas kozben bezarult. Torolve a listarol.\r\n", 
		    socket_list->nick,
		    ERROR_COMMAND, 
		    socket_list->ID,
		    ERROR_CODE_READ_SOCKETCLOSED);
	    FIFO_add_item(&IRC_data_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

/*socket eltavolitasa a listabol*/
	    remove_socket(&socket_list, &sock_number, socket_list);

	    printf( "*** Socket olvasas kozben bezarult.\n" );
	
	    i--; /*<----- hogy ezen socket helyere kerulo sockettel is foglalkozzunk*/
	 }
	else socket_list = socket_list->next;
	    
	if (j) can_read = FALSE;

     }
 }


void piller_sock_input()
 {
    int i, byte_counter, client_socket; //i szamolja a socketeket
    boolean can_read = TRUE, socket_closed = FALSE;
    char buffer[512];
    char nick[9];
    char ID[ID_LENGTH+1];

    while (can_read)
     {
        can_read = FALSE;
     
	for (i=0; i < piller_sock_number; i++)
	 {
	    if ( FD_ISSET(piller_socket_list->fd, &readfs))
	     {
		if (piller_socket_list->port)
		 {
		    client_socket = accept(piller_socket_list->fd, NULL, NULL);
		    fcntl(client_socket, F_SETFL, (O_NDELAY | fcntl(client_socket,F_GETFL)) );
		    close(piller_socket_list->fd);
		    piller_socket_list->fd = client_socket;
		    piller_socket_list->port = 0;

		    if ( (piller_socket_list->partner)->port )
		     { 
			printf(
			"*** %s Piller kapcs (A vagy B) kiepitve. (%s)\n", piller_socket_list->nick, piller_socket_list->ID);
	
		     }
		    else
		     {
			printf(
			"*** %s Piller tunnel kiepitve. (%s)\n", piller_socket_list->nick, piller_socket_list->ID);
	
		     }
		 }
		else 
		 {
		    if 
		     ( 
			(byte_counter = read(piller_socket_list->fd, buffer, 512*sizeof(char) ))
			<=
			0
		     )
		     {

			if (
			    !byte_counter
			    ||
			    (
				(byte_counter < 0)	
			    &&
			    errno != EAGAIN
			    )
			) socket_closed = TRUE;
			    /*
			    (Socket bezarult olvasas kozben, esemeny
			    kezelese kesobb, elotte 
			    a mar megkapott adatokat azert elkuldjuk.)*/
		     }

		    if ( byte_counter > 0 ) /*ha egyaltalan olvastam valamit*/
		     {
			can_read = TRUE;
		    
			FIFO_add_item(&((piller_socket_list->partner)->output_FIFO), 
			    buffer, 
			    byte_counter * sizeof(char));
		     }
		 }
	     }
	    
	    if (socket_closed)
	     {
		socket_closed = FALSE;
		/*
		Socket bezarult olvasas kozben! (A mar meglevo adatokat
		mar elkuldtuk.)*/

/*figyelmeztetes IRC-en*/

		sprintf(IRC_out_buffer,
		    "PRIVMSG %s :%s %s %s A piller socket olvasas kozben bezarult. Torolve a listarol.\r\n", 
		    piller_socket_list->nick,
		    PILLERERROR_COMMAND, 
		    piller_socket_list->ID,
		    PILLERERROR_CODE_PILLERREAD_SOCKETCLOSED);
		FIFO_add_item(&IRC_data_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** Piller socket olvasas kozben bezarult.\n" );
	
/*socket eltavolitasa a listabol*/
		strcpy(nick, piller_socket_list->nick);
		strcpy(ID, piller_socket_list->ID);
		while ( remove_socket( &piller_socket_list, &piller_sock_number, find_socket( piller_socket_list, piller_sock_number, ID, nick ) ) ) { }

		i--; /*<----- hogy ezen socket helyere kerulo sockettel is foglalkozzunk*/
	     }
	    else piller_socket_list = piller_socket_list->next;
	 }
     }
 }


void stdin_input()
 {
    int byte_counter; /*byte_counter szamolja az olvasott karaktereket*/
    char stdin_buffer[513];


    if 
     ( 
	(byte_counter = read(STDIN_FILENO, stdin_buffer, sizeof(char)*510 ) )
	==
	510
     )
     {
	while ( read(STDIN_FILENO, &temp_char, sizeof(char) ) > 0 ) {}
     }
    stdin_buffer[byte_counter] = '\r';				    
    stdin_buffer[byte_counter+1] = '\n';				    
    stdin_buffer[byte_counter+2] = '\0';				    

#ifdef DEBUG
printf("\r\nstdinput_entered\r\n");
#endif

    if ( !(stdin_command_processing(stdin_buffer)) && verbose)
     {
#ifdef DEBUG
printf("\r\nparancskiiras\r\n");
#endif
	FIFO_add_item(&IRC_controller_FIFO, 
	    stdin_buffer, 
	    strlen(stdin_buffer) * sizeof(char));
	    
     }	    
 }
 


void sock_output()
 {
    int i; //i a socket counter

	
    for (i=0; i < sock_number; i++)
     {
	if (socket_list->fd && FD_ISSET(socket_list->fd, &writefs))
	    if ( FIFO_flush( &(socket_list->output_FIFO), socket_list->fd, FALSE, NULL) )
	     {
		/*
		PANIC!!!! socket iras kozben bezarult*/

//figyelmeztetes IRC-en

		sprintf(IRC_out_buffer,
		    "PRIVMSG %s :%s %s %s A socket iras kozben bezarult. Torolve a listarol.\r\n", 
		    socket_list->nick,
		    ERROR_COMMAND,
		    socket_list->ID,
		    ERROR_CODE_WRITE_SOCKETCLOSED);
		FIFO_add_item(&IRC_data_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** Socket iras kozben bezarult.\n" );
	
//socket eltavolitasa a listabol
		remove_socket(&socket_list, &sock_number, socket_list);
		
		i--;
		continue;
	     }

	socket_list = socket_list->next;
     }
 }


void piller_sock_output()
 {
    int i; /*i a socket counter, j a bufferen belul mutat*/
    char nick[9];
    char ID[ID_LENGTH+1];
	
    for (i=0; i < piller_sock_number; i++)
     {
	if (FD_ISSET(piller_socket_list->fd, &writefs))
	    if ( FIFO_flush( &(piller_socket_list->output_FIFO), piller_socket_list->fd, FALSE, NULL) )
	     {
		/*
		PANIC!!!! socket iras kozben bezarult*/


/*figyelmeztetes IRC-en*/

		sprintf(IRC_out_buffer,
		    "PRIVMSG %s :%s %s %s A piller socket iras kozben bezarult. Torolve a listarol.\r\n", 
		    piller_socket_list->nick,
		    PILLERERROR_COMMAND, 
		    piller_socket_list->ID,
		    PILLERERROR_CODE_PILLERWRITE_SOCKETCLOSED);
		FIFO_add_item(&IRC_data_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** Piller socket iras kozben bezarult.\n" );
	
/*socket eltavolitasa a listabol*/
		strcpy(nick, piller_socket_list->nick);
		strcpy(ID, piller_socket_list->ID);
		while ( remove_socket( &piller_socket_list, &piller_sock_number, find_socket( piller_socket_list, piller_sock_number, ID, nick ) ) ) { }


		i--;
		continue;
	     }

	piller_socket_list = piller_socket_list->next;
     }
 }


void initialize_IRC_connection()
 {
 		
		make_IRC_connection = FALSE;
		connection_counter = reconnect * reconnections;	
//initializing IRC connection
//inicializalas	
		sprintf(IRC_out_buffer, "USER %s 0 * :%s\r\n", IRC_nick, IRC_real_name);
		FIFO_add_item(&IRC_controller_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** IRC kapcsolat inicializalasa.\n" );
	
		sprintf(IRC_out_buffer, "NICK %s\r\n", IRC_nick);
		FIFO_add_item(&IRC_controller_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** IRC nick megadas.\n" );
 }

boolean IRC_output()
 {
    int optval, optlen;
    
    optlen = sizeof(optval);
    if (connection_state == -4)
     {
     	getsockopt(IRC_sock_fd, SOL_SOCKET, SO_ERROR, &optval, &optlen);
//On success, zero is returned.  On error, -1 is returned, and errno is set appropriately.     
     	
/*The socket is non-blocking and the connection cannot  be  completed  immediately.
              It is possible to select(2) or poll(2) for completion by selecting the socket for
              writing. After select  indicates  writability,  use  getsockopt(2)  to  read  the
              SO_ERROR  option  at level SOL_SOCKET to determine whether connect completed suc-
              cessfully (SO_ERROR is zero) or unsuccessfully (SO_ERROR  is  one  of  the  usual
              error codes listed here, explaining the reason for the failure).
*/	
	connection_state = 0;
	if (optval) return FALSE;

	initialize_IRC_connection();
     	return TRUE; //Siker eseten!!!!!!!!!!!!!!!!!!!!
     }
    else 
    if 
     (
	FIFO_flush( &IRC_controller_FIFO, IRC_sock_fd, (boolean) verbose, NULL)
	||
	(
	    data_tr_enabled
	    &&
	    (	
		!IRC_controller_FIFO
		&&
		(
		    difftime(time((time_t *)NULL), w_time) >= delay
		    &&
		    (FIFO_write( &IRC_data_FIFO, IRC_sock_fd, (boolean) verbose, &w_time) == -1)
		)
	    )
	)
     )
     {
	/*
	PANIC!!!! IRC socket iras kozben bezarult
	*/
	return FALSE;     
     }
/* sikerult az IRC vonalra kiirnunk*/     
     return TRUE;
 }


void cleanup(int sig)
 {
    while (sock_number)	remove_socket(&socket_list, &sock_number, socket_list);
    while (piller_sock_number)	remove_socket(&piller_socket_list, &piller_sock_number, piller_socket_list);
	     
    if (IRC_sock_fd > 0) close(IRC_sock_fd);

    if (sig) printf("\n*** Signal: %d\n", sig);

    exit(sig);
 }


int parse_config_file(char* name, char* buffer, unsigned int buffer_size, char* default_value)
 {
    int	fd, j, i, k;
    unsigned short	state = 0;
    char read_buffer[513];


    strcpy(buffer, default_value);
    
    if ( (fd = open(CONFIG_FILE_NAME, O_RDONLY)) >= 0 )
     {
        while ((j = read(fd, read_buffer, sizeof(read_buffer)) ) > 0)
	 {
	    for (i = 0; i < j; i++)
	     {
#ifdef DEBUG_CON
printf("%c", read_buffer[i]);		
#endif
		switch(read_buffer[i])
		 {
		    case '=':
		     {
			if (state == 1)
			 {
			    if (name[k]) state = 3;
			    else
			     {
			      state = 2;
			      k = 0;
			      buffer[k] = '\0';
			     }
			 }
			else if (state == 2) 
			 {
			    if ( (k+1)*sizeof(char) >= buffer_size) 
				return -1;
			    else
			     {
				buffer[k] = read_buffer[i];
				k++;
				buffer[k] = '\0';
			     }
			 }
			break;
		     }
		    case '\n':
		     {
			if (state == 2) return 0;
			state = 0;
			break;
		     }
		    case '#':
		     {
			if (state == 0) state = 3;
			else if (state == 1)
			 {
			    if ( k*sizeof(char) >= strlen(name) ) state = 3;
			    else
			     {
				if (name[k] != read_buffer[i]) state = 3;
				k++;
			     }
			 }

			else if (state == 2)
			 {
			    if ( (k+1)*sizeof(char) >= buffer_size ) 
				return -1;
			    else
			     {
				buffer[k] = read_buffer[i];
			        k++;
				buffer[k] = '\0';
			     }
			 }

			break;
		     }
		    default:
		     {
			if (state == 0)
			 {
			    state = 1;
			    k = 0;
			    if ( k*sizeof(char) >= strlen(name) ) state = 3;
			    else
			     {
				if (name[k] != read_buffer[i]) state = 3;
				k++;
			     }
			 }
			else if (state == 1)
			 {
			    if ( k*sizeof(char) >= strlen(name) ) state = 3;
			    else
			     {
				if (name[k] != read_buffer[i]) state = 3;
				k++;
			     }
			 }
			else if (state == 2)
			 {
			    if ( (k+1)*sizeof(char) >= buffer_size ) 
				return -1;
			    else
			     {
				buffer[k] = read_buffer[i];
				k++;
				buffer[k] = '\0';
			     }
			 }
		     }
		 }
	     }
	 }
	 
#ifdef DEBUG_CONFIG

	printf("while mukodott: %d\n", j);
	printf("fd: %d\n", i);
#endif
	close(fd);

	if (j < 0) 
	 {
	    printf("*** Config file olvasasi hiba. (%s)\n", CONFIG_FILE_NAME );
	    return 2;
	 }

	return 0;
     }
    else
     {
	printf("*** Config file-t nem talaltam. (%s)\n", CONFIG_FILE_NAME );
	return 1;
     }
 }


int parse_config_file_ul(char* name, unsigned short* integer, unsigned short default_value)
 {
    int	i;
    char buffer[513];
    char** endptr = NULL;

    
    if (
	((i = parse_config_file(name, buffer, sizeof(buffer), "")) > 0)
	||
	(!buffer[0])
	)
     {
	*integer = default_value;
	return i;
     }
    
    *integer = strtoul(buffer, endptr, 0);

#ifdef DEBUG_CON
printf("name: %s\tstring: %s\tinteger: %d\n", name, buffer, *integer);
#endif
    
    if (endptr)
     {
	if (*endptr == buffer)
	 {
//	erteket nem lehetett atalakitani
	    *integer = default_value;
	    return 3;
	 }

	if (**endptr)
	 {
//        erteket csak bizonyos reszig lehetett atalakitani
	    return -2+i;
	 }
     }
    
    return i;
 }


int parse_config_file_bool(char* name, boolean* boolean_value, boolean default_value)
 {
    int	i;
    char buffer[2];

    
    if (
	((i = parse_config_file(name, buffer, sizeof(buffer), "")) > 0)
	||
	(!buffer[0])
	)
     {
	*boolean_value = default_value;
	return i;
     }
    
    *boolean_value = (boolean) (buffer[0] - '0');

    return i;
 }





int main(int argc, char *argv[])
 {
    int i, max_fd;
    boolean handle_sock, time_out;

//logo kiiras
    if ( (i = open(LOGO_NAME, O_RDONLY)) >= 0 )
     {
        while ((max_fd = read(i, stdout_buffer, sizeof(stdout_buffer)) ) > 0)
	    write(STDOUT_FILENO,
				stdout_buffer,
				max_fd );
	if (max_fd < 0) printf("*** Logo kiirasi hiba. (%s)\n", LOGO_NAME );
	
	close(i);
     }
    else printf("*** Logot nem talaltam. (%s)\n", LOGO_NAME );

//config file feldolgozas
    parse_config_file_ul("port", &IRC_port, DEFAULT_PORT);

    parse_config_file("other_nick", IRC_other_nick, 
		    sizeof(IRC_other_nick), DEFAULT_OTHER_NICK);

    parse_config_file_ul("chunk_size", &chunk_size, DEFAULT_CHUNK_SIZE);
    if ((chunk_size > 390) || (chunk_size < 1)) chunk_size = DEFAULT_CHUNK_SIZE;

    parse_config_file_bool("reconnect", &reconnect, DEFAULT_RECONNECT);

    parse_config_file_ul("reconnections", &reconnections, DEFAULT_RECONNECTIONS);
    if ((reconnections > 100) || (reconnections < 1)) chunk_size = DEFAULT_RECONNECTIONS;

    parse_config_file_bool("limit_reconnections", &limit_reconnections, 
	DEFAULT_LIMIT_RECONNECTIONS);

    parse_config_file("command_char", stdout_buffer, 
		    sizeof(char)*2, COMMAND_CHAR);
    command_char = stdout_buffer[0];

/*

#define DATA_COMMAND "D"
char data_command[3];


#define OUT_COMMAND "O"
char out_command[3];

#define CLOSE_COMMAND "C"
char close_command[3];

#define ERROR_COMMAND "E"
char error_command[3];

#define REPORT_COMMAND "R"
char report_command[3];

#define TUNNEL_COMMAND "T"
char tunnel_command[3];

#define PILLEROUT_COMMAND "PO"
char pillerout_command[3];

#define PILLERERROR_COMMAND "PE"
char pillererror_command[3];

#define PILLERREPORT_COMMAND "PR"
char pillerreport_command[3];

#define EXTERNAL_TUNNEL_COMMAND "E"
char external_tunnel_command[3];

#define LIST_COMMAND "list"
char list_command[10];


#define DELAY_COMMAND "delay"
char delay_command[10];

#define VERBOSE_COMMAND "verbose"
char verbose_command[10];

#define QUIT_COMMAND "quit"
char quit_command[10];

#define SERVER_COMMAND "server"
char server_command[10];

*/


    
    parse_config_file_ul("delay", &delay, DEFAULT_DELAY);
//    delay = DEFAULT_DELAY;

    parse_config_file_bool("verbose", &verbose, VERBOSE);

//    verbose = (unsigned short) VERBOSE;

/*    strcpy(IRC_nick, DEFAULT_NICK);
    strcpy(IRC_server, DEFAULT_SERVER);
    strcpy(IRC_real_name, DEFAULT_REAL_NAME);*/

    parse_config_file("nick", IRC_nick, sizeof(IRC_nick), DEFAULT_NICK);
    parse_config_file("server", IRC_server, sizeof(IRC_server), DEFAULT_SERVER);
    parse_config_file("real_name", IRC_real_name, sizeof(IRC_real_name), DEFAULT_REAL_NAME);

//configuracio kiiratasa
    list_conf();
#ifdef DEBUG_CONFIG
printf("nick: %s\n", IRC_nick);
printf("server: %s\n", IRC_server);
printf("real_name: %s\n", IRC_real_name);

#endif

//idozites

    time(&w_time);
    time(&s_time);

    tv.tv_sec = delay;
    tv.tv_usec = 0;

//szignalok
    signal(SIGINT, cleanup);
    signal(SIGHUP, cleanup);
    signal(SIGKILL, cleanup);
    signal(SIGQUIT, cleanup);
    signal(SIGSTOP, cleanup);
    signal(SIGSEGV, cleanup);



//getting command line parameters
//parameterek olvasasa
    if (argc > 1)
     {
	for (i = 1; i < argc; i++)
	    if (strchr(argv[i], '.')) 
		strcpy(IRC_server, argv[i]);
	    else 
		if (atoi(argv[i]) > 255)
		    IRC_port = atoi(argv[i]);
		else
		    strcpy(IRC_nick, argv[i]);
     }

    connection_counter = reconnections;    

/*main loop*/
    while (work) 
     {
	if (make_IRC_connection && connection_counter && (connection_state != -4))
	 {
//getting connected
//kapcsolodas
	    printf("\n*** Csatlakozni probalok.\n");

	    printf("*** IRC kapcsolat:\n\tszerver: %s\n\tport: %d\n", IRC_server, IRC_port );

	    if ((connection_state = make_connection(IRC_server, IRC_port, &IRC_sock_fd)) < 0)
	     {
		if (connection_state >= -3)
		{
#ifdef DEBUG_CON		    
		    printf("connection_counter: %d", connection_counter);
#endif		    
		    connection_counter = reconnect * (connection_counter - limit_reconnections);
/*		    if (connection_counter)
		    {*/
		         if ( limit_reconnections ) printf("*** %d-szer probalkozom meg.\n", connection_counter);
//		    }
				
		    switch (connection_state)
		 	{
		 		case  -1:
			 	{
		    			printf( "*** Kapcsolodas sikertelen: DNS hiba.\n");
			   		break;
			 	}
		 		case  -2:
			 	{
		    			printf( "*** Kapcsolodas sikertelen: Socket hiba.\n");
			   		break;
			 	}
		 		case  -3:
			 	{
		    			printf( "*** Kapcsolodas sikertelen: Kapcsolodasi hiba.\n");
			   		break;
			 	}
		 
		 	}

		}
			    
	     } 	    
/*	    if ((IRC_sock_fd = make_connection(IRC_server, IRC_port)) < 0) 
	     {
		printf( "*** Kapcsolodas sikertelen\n");
		connection_counter = reconnect * (connection_counter - limit_reconnections);
		if (connection_counter)
		 {
		    if ( limit_reconnections ) printf("*** %d-szer probalkozom meg.\n\n", connection_counter);
		 }

	     }*/
	    else
	     {
		initialize_IRC_connection();
		
/*		make_IRC_connection = FALSE;
		connection_counter = reconnect * reconnections;	
//initializing IRC connection
//inicializalas	
		sprintf(IRC_out_buffer, "USER %s 0 * :%s\r\n", IRC_nick, IRC_real_name);
		FIFO_add_item(&IRC_controller_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** IRC kapcsolat inicializalasa.\n" );
	
		sprintf(IRC_out_buffer, "NICK %s\r\n", IRC_nick);
		FIFO_add_item(&IRC_controller_FIFO, 
		    IRC_out_buffer, 
		    strlen(IRC_out_buffer) * sizeof(char));

		printf( "*** IRC nick megadas.\n" );*/
	
	     }
	 }

	FD_ZERO(&writefs);
	FD_ZERO(&readfs);
	tv.tv_sec = delay;
	tv.tv_usec = 0;


//filling write list
//irasi lista feltoltese
	if (IRC_sock_fd > 0)
	 {
	    if (
	    	(connection_state == -4) //A kapcsolodas megindult, de nem fejezodott be
		||
		(IRC_controller_FIFO 
		|| 
		(
		    IRC_data_FIFO 
		    && 
		    (difftime(time((time_t *)NULL), w_time) >= delay)
		))
	    ) FD_SET(IRC_sock_fd, &writefs);

	 }    
	 
	for (i=0; i < sock_number; i++)
	 {
	    if ( socket_list->output_FIFO && (socket_list->fd && !socket_list->port)) FD_SET(socket_list->fd, &writefs);
	    socket_list = socket_list->next;
	 }

	for (i=0; i < piller_sock_number; i++)
	 {
	    if ( piller_socket_list->output_FIFO && !piller_socket_list->port) FD_SET(piller_socket_list->fd, &writefs);
	    piller_socket_list = piller_socket_list->next;
	 }

//filling read list, searching for the highest file descriptor number
//olvasasi lista feltoltese, legnagyobb fd keresese
	if (IRC_sock_fd > 0)
	 {
	    max_fd = (IRC_sock_fd > STDIN_FILENO) ? IRC_sock_fd : STDIN_FILENO;
	    FD_SET(IRC_sock_fd, &readfs);

	    handle_sock = FALSE;

	    for (i=0; i < sock_number; i++)
	     {
	     
//	        handle_sock = TRUE;
		if ( socket_list->fd)
		 {
		    if (		   
		    (
			socket_list->port
			||
		        difftime(time((time_t *)NULL), s_time) >= delay 
		    )
		    )

		     {
			FD_SET(socket_list->fd, &readfs);
	    		max_fd = (max_fd > socket_list->fd) ? max_fd : socket_list->fd;
//			handle_sock = FALSE;
		     }
		    else handle_sock = TRUE;
		 }
		socket_list = socket_list->next;

	     }
	 }
	else max_fd = STDIN_FILENO;

	FD_SET(STDIN_FILENO, &readfs);

	for (i=0; i < piller_sock_number; i++)
	 {
	    FD_SET(piller_socket_list->fd, &readfs);
	    max_fd = (max_fd > piller_socket_list->fd) ? max_fd : piller_socket_list->fd;

	    piller_socket_list = piller_socket_list->next;
	 }

	time_out = (
			(make_IRC_connection && connection_counter)
			||
			(
			    (IRC_sock_fd > 0) 
			    && 
			    delay 
			    && 
			    (IRC_data_FIFO || handle_sock)
			)
		    );

#ifdef DEBUG3
//printf("%d\n", time_out);	
#endif
	select(max_fd+1, &readfs, &writefs, NULL, time_out ? &tv : NULL);

//analyzing write table
//irasi tabla vizsgalat

	sock_output();
	piller_sock_output();		

	if (IRC_sock_fd > 0)
	 {

	    if
	        ( 
		    (FD_ISSET(IRC_sock_fd, &writefs)) 
		    && 
		    !(IRC_output()) 
		 ) 
	     {
		IRC_sock_fd = -1;
		make_IRC_connection = TRUE;
		data_tr_enabled = FALSE;
	
		while (IRC_controller_FIFO) FIFO_del_first_item(&IRC_controller_FIFO);

		printf( "*** IRC kapcsolat zarult iras kozben.\n" );
	
	     }
	 }
//analyzing read table
//olvasasi tabla kiertekelese
	if ( FD_ISSET(STDIN_FILENO, &readfs) ) stdin_input();

	if (IRC_sock_fd > 0)
	 {

	    if
	     (
		( FD_ISSET(IRC_sock_fd, &readfs))
		&&
		!(IRC_input())
	     ) 
	     {
		IRC_sock_fd = -1;
		make_IRC_connection = TRUE;
		data_tr_enabled = FALSE;
	
		while (IRC_controller_FIFO) FIFO_del_first_item(&IRC_controller_FIFO);

		printf( "*** IRC kapcsolat zarult olvasas kozben.\n" );
	
	     }
	 }

	sock_input();
	piller_sock_input();

     }

    cleanup(0);
//a program soha nem er el idaig
    exit(0);
 }

/* EOF */
